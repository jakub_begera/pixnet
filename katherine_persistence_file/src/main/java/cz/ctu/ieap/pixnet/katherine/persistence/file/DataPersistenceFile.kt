package cz.ctu.ieap.pixnet.katherine.persistence.file

import cz.ctu.ieap.pixnet.commons.model.AbstractDataFrame
import cz.ctu.ieap.pixnet.commons.utils.EasyLogger
import cz.ctu.ieap.pixnet.handler.detector_data_persistence_intf.DataPersistence
import cz.ctu.ieap.pixnet.kathrine_commons.model.*
import cz.ctu.ieap.pixnet.kathrine_commons.model.DataFrameHeaders.*
import cz.ctu.ieap.pixnet.kathrine_commons.utils.LsbMsbPacketMatcher
import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.BlockingQueue

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class DataPersistenceFile : DataPersistence {

    private var queue: BlockingQueue<AbstractDataFrame>? = null
    private val logger = EasyLogger.get(this.javaClass)
    private var isRunning: Boolean = false
    private var acqMode: DetectorConfig.AcqMode? = null
    private var callback: DataPersistence.Callback? = null
    private var detectorConfig: DetectorConfig? = null
    private val lsbMsbPacketMatcher = LsbMsbPacketMatcher<KeysLsbMsbFrames, DataFrame>()
    private var currentPixelTimestampCorrection: Long = 0
    private var numbeOfFrameInMeasurement = 0L

    private var file: File? = null
    private var writer: BufferedWriter? = null

    enum class KeysLsbMsbFrames {
        START_OF_FRAME_TIMESTAMP,
        END_OF_FRAME_TIMESTAMP,
    }

    companion object {

        private const val PIXEL_TIMESTAMP_OFFSET_ENABLED = true
        private val dateFormat = SimpleDateFormat("yyyy.MM.dd HH:mm:ss.SSSZ")
        private val dateFormatFile = SimpleDateFormat("yyyy_MM_dd-HH_mm_ss")
        private const val PRINT_RAW_PAYLOAD_TOO = false

        init {
            dateFormat.timeZone = TimeZone.getTimeZone("UTC")
            dateFormatFile.timeZone = TimeZone.getTimeZone("UTC")
        }
    }

    override fun setDataFrameQueue(queue: BlockingQueue<AbstractDataFrame>) {
        this.queue = queue
    }

    override fun start() {
        if (queue == null) throw RuntimeException("Set data queue at first!")

        isRunning = true
        val queueObserverThread = Thread {
            while (isRunning) {
                try {
                    val frame = queue!!.take()
                    processFrame(frame)
                } catch (e: InterruptedException) {
                    logger.e(e, "Can't peak frame from queue")
                }

            }
        }
        queueObserverThread.start()
    }

    override fun stop() {
        isRunning = false
    }

    override fun setCallback(callback: DataPersistence.Callback) {
        this.callback = callback
    }

    override fun setDetectorConfig(config: String) {
        detectorConfig = DetectorConfig.fromYaml(config, callback!!.classLoader)
    }

    private fun openNewFile() {
        if (file != null) closeFile()
        file = File(String.format("%s/%s_%s.txt", detectorConfig!!.outputDir, detectorConfig!!.name,
                dateFormatFile.format(Date(System.currentTimeMillis()))))
        try {
            file?.parentFile?.mkdirs()
            writer = BufferedWriter(FileWriter(file))
        } catch (e: IOException) {
            logger.e(e, "File opening fail...")
        }

    }

    private fun closeFile() {
        if (writer != null) {
            try {
                writer!!.close()
            } catch (e: IOException) {
                logger.e(e, "File closing fail...")
            }

        }
        file = null
    }

    private fun processFrame(frame: AbstractDataFrame) {
        logger.d("New frame: %s", frame.toString())
        if (frame is DataFrame) {
            processDataFrame(frame)
        } else if (frame is ControlFrame) {
            processControlFrame(frame)
        } else if (frame is AcqConfigurationFrame) {
            processAcqConfigurationFrame(frame)
        } else {
            logger.e("Unknown data frame class: %s", frame.javaClass.name)
        }
    }

    private fun processControlFrame(frame: ControlFrame) {
        when (frame.type) {
            ControlFrame.Type.MEASUREMENT_STARTED -> {
                numbeOfFrameInMeasurement = 0L
                openNewFile()
                printComment("Start of measurement: %s", dateFormat.format(Date(frame.createdTimestamp)))
            }
            ControlFrame.Type.MEASUREMENT_FINISHED -> {
                closeFile()
                printComment("Finish of measurement: %s", dateFormat.format(Date(frame.createdTimestamp)))
            }
            ControlFrame.Type.PERSIST_VALUE -> print(frame.payload!!.toString())
            else -> logger.e("ControlFrame type %s not implemented", frame.type.name)
        }
    }

    private fun processAcqConfigurationFrame(frame: AcqConfigurationFrame) {
        acqMode = frame.acqMode
        printComment("Acq start timestamp: %d", frame.acqStartTimestamp)
        printComment("Chip ID: %s", frame.chipID)
        printComment("Acq mode: %s", frame.acqMode.mode)
        printComment("Fast VCO enabled: %b", frame.acqMode.fastVCOEnabled)
        printComment("Readout mode: %s", frame.readOutMode.mode)
        printComment("Bias: %f", frame.bias)
        printComment("Temperature readout: %f", frame.temperatureReadout)
        printComment("Temperature sensor: %f", frame.temperatureSensor)
        printComment("Payload older: PIX_ID\t%s", frame.acqMode.payloadOlder)
    }

    private fun processDataFrame(frame: DataFrame) {
        if (frame.hasMeasurementData()) {
            if (acqMode != null) {
                print(frame.formatMeasuredData(acqMode!!, currentPixelTimestampCorrection))
                if (PRINT_RAW_PAYLOAD_TOO) {
                    print(frame.getRawBites())
                }
            } else {
                logger.e("Acq mode not set. Skipping frame: %s", frame.toString())
            }
        } else if (frame.isLsbOrMsb) {
            val lsbMsb: Pair<DataFrame, DataFrame>?
            when (frame.header) {
                START_OF_FRAME_TIMESTAMP_LSB, DataFrameHeaders.START_OF_FRAME_TIMESTAMP_MSB -> {
                    lsbMsb = if (frame.header === START_OF_FRAME_TIMESTAMP_LSB) {
                        lsbMsbPacketMatcher.putLsbAndCheck(KeysLsbMsbFrames.START_OF_FRAME_TIMESTAMP, frame)
                    } else {
                        lsbMsbPacketMatcher.putMsbAndCheck(KeysLsbMsbFrames.START_OF_FRAME_TIMESTAMP, frame)
                    }
                    if (lsbMsb != null) {
                        // we have a match
                        val startOfFrameTimestamp = DataFrame.getValueFromLsbMsb(lsbMsb.first, lsbMsb.second)
                        printComment("Start of frame timestamp: %d", startOfFrameTimestamp)
                    }
                }
                DataFrameHeaders.END_OF_FRAME_TIMESTAMP_LSB, DataFrameHeaders.END_OF_FRAME_TIMESTAMP_MSB -> {
                    lsbMsb = if (frame.header === END_OF_FRAME_TIMESTAMP_LSB) {
                        lsbMsbPacketMatcher.putLsbAndCheck(KeysLsbMsbFrames.END_OF_FRAME_TIMESTAMP, frame)
                    } else {
                        lsbMsbPacketMatcher.putMsbAndCheck(KeysLsbMsbFrames.END_OF_FRAME_TIMESTAMP, frame)
                    }
                    if (lsbMsb != null) {
                        // we have a match
                        val endOfFrameTimestamp = DataFrame.getValueFromLsbMsb(lsbMsb.first, lsbMsb.second)
                        printComment("End of frame timestamp: %d", endOfFrameTimestamp)
                    }
                }
            }
        } else if (frame.header === PIXEL_TIMESTAMP_OFFSET && PIXEL_TIMESTAMP_OFFSET_ENABLED) {
            currentPixelTimestampCorrection = frame.longValue
        } else {
            if (frame.header == CURRENT_FRAME_FINISHED) {
                printComment("Frame ${numbeOfFrameInMeasurement++} finished")
            }
            printComment(frame.toString())
        }
    }

    private fun printComment(s: String, vararg args: Any) {
        print("# $s", *args)
    }

    private fun print(s: String, vararg args: Any) {
        val message: String = String.format(s, *args)
        if (writer != null) {
            try {
                writer!!.write(message)
                writer!!.newLine()
                writer!!.flush()
            } catch (e: IOException) {
                logger.e(e, "Data writing fail...")
            }

        }
    }

}
