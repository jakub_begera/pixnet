package cz.ctu.ieap.pixnet.commons.exceptions

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class DetectorException : Exception {

    constructor(message: String, vararg args: Any) : super(String.format(message, *args)) {}

    constructor(cause: Throwable, message: String, vararg args: Any) : super(String.format(message, *args), cause) {}
}
