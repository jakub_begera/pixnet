package cz.ctu.ieap.pixnet.commons.model

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
abstract class AbstractDataFrame(val createdTimestamp: Long)
