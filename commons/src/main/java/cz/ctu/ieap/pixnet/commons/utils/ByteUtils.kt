package cz.ctu.ieap.pixnet.commons.utils

import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.util.*

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
object ByteUtils {

    fun intToBytes(`val`: Int): ByteArray {
        return ByteBuffer.allocate(4).putInt(`val`).array()
    }

    fun longToBytes(`val`: Long): ByteArray {
        return ByteBuffer.allocate(8).putLong(`val`).array()
    }

    fun floatToBytes(`val`: Float): ByteArray {
        return ByteBuffer.allocate(4).putFloat(`val`).array()
    }

    fun doubleToBytes(`val`: Double): ByteArray {
        return ByteBuffer.allocate(8).putDouble(`val`).array()
    }

    fun bytesToInt(bytes: ByteArray, order: ByteOrder = ByteOrder.BIG_ENDIAN): Int {
        var bytes = bytes
        assert(bytes.size in 1..4)
        if (bytes.size < 4) {
            val pom = ByteArray(4)
            System.arraycopy(bytes, 0, pom, 4 - bytes.size, bytes.size)
            bytes = pom
        }
        return ByteBuffer.wrap(bytes).order(order).int
    }

    fun bytesToLong(bytes: ByteArray, order: ByteOrder = ByteOrder.BIG_ENDIAN): Long {
        var bytes = bytes
        assert(bytes.size in 1..8)
        if (bytes.size < 8) {
            val pom = ByteArray(8)
            if (order == ByteOrder.BIG_ENDIAN) {
                System.arraycopy(bytes, 0, pom, 8 - bytes.size, bytes.size)
            } else {
                System.arraycopy(bytes, 0, pom, 0, bytes.size)
            }
            bytes = pom
        }
        return ByteBuffer.wrap(bytes).order(order).long
    }

    fun bytesToFloat(bytes: ByteArray): Float {
        assert(bytes.size == 4)
        return ByteBuffer.wrap(bytes).float
    }

    fun bytesToDouble(bytes: ByteArray): Double {
        assert(bytes.size == 8)
        return ByteBuffer.wrap(bytes).double
    }

    fun getLastNBites(b: Byte, n: Int): String {
        val s = String.format("%8s", Integer.toBinaryString(b.toInt() and 0xFF)).replace(' ', '0')
        return s.substring(8 - n)
    }

    fun reverse(arr: ByteArray): ByteArray {
        val pom = ByteArray(arr.size)
        for (i in arr.indices) {
            pom[arr.size - 1 - i] = arr[i]
        }
        return pom
    }

    fun getMsbFromLong(value: Long): ByteArray {
        val bytes = longToBytes(value)
        return Arrays.copyOfRange(bytes, 0, 4)
    }

    fun getLsbFromLong(value: Long): ByteArray {
        val bytes = longToBytes(value)
        return Arrays.copyOfRange(bytes, 4, 8)
    }

    fun bytesToStringBites(arr: ByteArray, separator: String): String {
        val sb = StringBuilder()

        for (i in arr.indices.reversed()) {
            val s = Integer.toBinaryString(arr[i].toInt() and 0xFF).replace(' ', '0')
            for (j in 0 until s.length) {
                sb.append(s[j])
                sb.append(separator)
            }
        }

        return sb.toString()
    }

}
