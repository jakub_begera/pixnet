package cz.ctu.ieap.pixnet.commons.utils

import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class EasyLogger constructor(c: Class<*>) {

    private val logger: Logger = LoggerFactory.getLogger(c)

    fun d(message: String, vararg args: Any) {
        logger.debug(String.format(message, *args))
    }

    fun d(t: Throwable, message: String, vararg args: Any) {
        logger.debug(String.format(message, *args), t)
    }

    fun i(message: String, vararg args: Any) {
        logger.info(String.format(message, *args))
    }

    fun i(t: Throwable, message: String, vararg args: Any) {
        logger.info(String.format(message, *args), t)
    }

    fun w(message: String, vararg args: Any) {
        logger.warn(String.format(message, *args))
    }

    fun w(t: Throwable, message: String, vararg args: Any) {
        logger.warn(String.format(message, *args), t)
    }

    fun e(message: String, vararg args: Any) {
        logger.error(String.format(message, *args))
    }

    fun e(t: Throwable, message: String, vararg args: Any) {
        logger.error(String.format(message, *args), t)
    }

    companion object {

        operator fun get(c: Class<*>): EasyLogger {
            return EasyLogger(c)
        }
    }

}
