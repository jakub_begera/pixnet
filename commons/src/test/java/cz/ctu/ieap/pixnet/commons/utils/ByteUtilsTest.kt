package cz.ctu.ieap.pixnet.commons.utils

import org.junit.Assert.assertEquals
import org.junit.Test
import java.nio.ByteBuffer

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class ByteUtilsTest {

    @Test
    fun intToBytes() {
        val bytes = ByteUtils.intToBytes(123)
        assertEquals(4, bytes.size.toLong())

        val anInt = ByteBuffer.wrap(bytes).int
        assertEquals(123, anInt.toLong())
    }

    @Test
    fun longToBytes() {
        val bytes = ByteUtils.longToBytes(Integer.MAX_VALUE.toLong() + 123L)
        assertEquals(8, bytes.size.toLong())

        val anLong = ByteBuffer.wrap(bytes).long
        assertEquals(Integer.MAX_VALUE.toLong() + 123L, anLong)
    }

    @Test
    fun floatToBytes() {
        val bytes = ByteUtils.floatToBytes(123.321f)
        assertEquals(4, bytes.size.toLong())

        val `val` = ByteBuffer.wrap(bytes).float
        assertEquals(123.321f, `val`, 0.01f)
    }

    @Test
    fun doubleToBytes() {
        val bytes = ByteUtils.doubleToBytes(123.321)
        assertEquals(8, bytes.size.toLong())

        val `val` = ByteBuffer.wrap(bytes).double
        assertEquals(123.321, `val`, 0.01)
    }

    @Test
    fun bytesToInt() {
        assertEquals(
                276,
                ByteUtils.bytesToInt(byteArrayOf(0, 0, 1, 20)).toLong()
        )
    }

    @Test
    fun bytesToLong() {
        assertEquals(
                276,
                ByteUtils.bytesToLong(byteArrayOf(0, 0, 0, 0, 0, 0, 1, 20))
        )
    }

    @Test(expected = AssertionError::class)
    fun bytesToLongOverflow() {
        ByteUtils.bytesToLong(byteArrayOf(0, 0, 1, 20, 0, 0, 1, 20, 0, 0, 1, 20))
    }


    @Test
    fun bytesToFloat() {
        assertEquals(
                1.1715392E-31,
                ByteUtils.bytesToFloat(byteArrayOf(12, 24, 19, 17)).toDouble(),
                0.00001
        )
    }

    @Test
    fun reverse() {
        val original = byteArrayOf(1, 2, 3, 4, 5)
        val expected = byteArrayOf(5, 4, 3, 2, 1)
        val reverse = ByteUtils.reverse(original)
        for (i in reverse.indices) {
            assertEquals(expected[i].toLong(), reverse[i].toLong())
        }
    }
}