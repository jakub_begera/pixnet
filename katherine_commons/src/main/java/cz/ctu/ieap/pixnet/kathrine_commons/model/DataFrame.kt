package cz.ctu.ieap.pixnet.kathrine_commons.model

import cz.ctu.ieap.pixnet.commons.exceptions.DetectorException
import cz.ctu.ieap.pixnet.commons.model.AbstractDataFrame
import cz.ctu.ieap.pixnet.commons.utils.ByteUtils
import cz.ctu.ieap.pixnet.kathrine_commons.model.DataFrameHeaders.*
import cz.ctu.ieap.pixnet.kathrine_commons.model.DataFrameHeaders.Companion.getById
import java.nio.ByteOrder
import java.util.*
import kotlin.experimental.and

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class DataFrame @Throws(DetectorException::class)
constructor(createdTimestamp: Long, var data: ByteArray) : AbstractDataFrame(createdTimestamp) {

    var header: DataFrameHeaders? = null

    init {
        if (data.size != 6) {
            throw DetectorException("Wrong data length")
        }
        val id = (data[5].toInt() shr 4) and 0x0f
        header = getById(id)
    }

    val coordinateX: Int
        get() = ((data[4].toInt() and 0x0f) shl 4) or ((data[3].toInt() and 0xf0) shr 4)

    val coordinateY: Int
        get() = ((data[5].toInt() and 0x0f) shl 4) or ((data[4].toInt() and 0xf0) shr 4)

    /**
     * Returns number from data on position 43..0
     */
    val longValue: Long
        get() {
            val bytes = Arrays.copyOfRange(data, 0, 5)
            bytes[4] = bytes[4] and 0x0f
            return ByteUtils.bytesToLong(bytes, ByteOrder.LITTLE_ENDIAN)
        }

    /**
     * Returns number from data on position 31..0
     */
    val intValueSigned: Int
        get() {
            val bytes = Arrays.copyOfRange(data, 0, 4)
            return ByteUtils.bytesToInt(bytes)
        }

    /**
     * Returns number from data on position 27..14
     */
    private val col3: Int
        get() {
            var value = (data[3].toInt() and 0x0f) shl 8
            value = value or (data[2].toInt() and 0xff)
            value = value shl 2
            value = value or ((data[1].toInt() and 0xc0) shr 6)
            return value
        }

    /**
     * Returns number from data on position 13..4
     */
    val col2: Int
        get() {
            var value: Int = data[1].toInt() and 0x3f
            value = value shl 4
            value = value or ((data[0].toInt() and 0xf0) shr 4)
            return value
        }

    /**
     * Returns number from data on position 3..0
     */
    val col1: Int
        get() = data[0].toInt() and 0x0f

    val isLsbOrMsb: Boolean
        get() = (header == START_OF_FRAME_TIMESTAMP_LSB
                || header == START_OF_FRAME_TIMESTAMP_MSB
                || header == END_OF_FRAME_TIMESTAMP_LSB
                || header == END_OF_FRAME_TIMESTAMP_MSB)

    fun getValue(value: Value): Int {
        return when (value) {
            DataFrame.Value.ToA, DataFrame.Value.iToT -> col3
            DataFrame.Value.ToT, DataFrame.Value.EventCounter -> col2
            DataFrame.Value.FastToA, DataFrame.Value.HitCounter -> col3
            else -> throw RuntimeException(String.format("Value not implemented - %s", value.name))
        }
    }

    fun pixelIndex(): Int {
        return 256 * coordinateY + coordinateX
    }

    override fun toString(): String {
        return when (header) {
            NEW_FRAME_ESTABLISHED, MEASUREMENT_ABORTED_NOTICE -> header!!.name
            PIXEL_MEASUREMENT_DATA -> String.format("[%s], coordinates: [%d,%d], values: [%d, %d, %d]", header!!.name,
                    coordinateX, coordinateY, col3, col2, col1)
            CURRENT_FRAME_FINISHED -> "Sent pixels: $longValue"
            NUMBER_OF_LOST_PIXELS -> "Lost pixels: $longValue"
            PIXEL_TIMESTAMP_OFFSET -> String.format("%s - %d", header!!.name, longValue)
            else -> String.format("[%s], %s", header!!.name, Arrays.toString(data))
        }
    }

    fun hasMeasurementData(): Boolean {
        return header == PIXEL_MEASUREMENT_DATA
    }

    fun formatMeasuredData(acqMode: DetectorConfig.AcqMode, currentPixelTimestampCorrection: Long): String {
        return when (acqMode) {

            DetectorConfig.AcqMode.Fast_VCO_ON_TOA_TOT, // ToA ToT FastToA
            DetectorConfig.AcqMode.Fast_VCO_OFF_TOA_TOT // ToA ToT HitCounter
            -> String.format("%5d\t%d\t%d\t%d", pixelIndex(), getToA(currentPixelTimestampCorrection, col3), col2, col1)

            DetectorConfig.AcqMode.Fast_VCO_OFF_EventCount_iToT // iToT EventCounter HitCounter
            -> String.format("%5d\t%d\t%d\t%d", pixelIndex(), col3, col2, col1)

            DetectorConfig.AcqMode.Fast_VCO_ON_TOA, // ToA --- FastToA
            DetectorConfig.AcqMode.Fast_VCO_OFF_TOA // ToA --- HitCounter
            -> String.format("%5d\t%d\t%d", pixelIndex(), getToA(currentPixelTimestampCorrection, col3), col1)

            DetectorConfig.AcqMode.Fast_VCO_ON_EventCount_iToT // iToT EventCounter ---
            -> String.format("%5d\t%d\t%d", pixelIndex(), col3, col2)

            else -> throw RuntimeException("Unknown acq mode: " + acqMode.name)
        }
    }

    fun getRawBites() = ByteUtils.bytesToStringBites(data, "\t")

    enum class Value {
        ToA,
        ToT,
        iToT,
        FastToA,
        EventCounter,
        HitCounter
    }

    private fun getToA(currentPixelTimestampCorrection: Long, offsetValue: Int): Long {
        return currentPixelTimestampCorrection * 16384 * 25 - (offsetValue * 1.5625).toLong()
    }

    companion object {

        const val LENGTH = 6

        fun getValueFromLsbMsb(lsb: DataFrame, msb: DataFrame): Long {
            val bytes = ByteArray(8)
            System.arraycopy(lsb.data, 0, bytes, 0, 4)
            System.arraycopy(msb.data, 0, bytes, 4, 4)
            return ByteUtils.bytesToLong(bytes, ByteOrder.LITTLE_ENDIAN)
        }
    }
}
