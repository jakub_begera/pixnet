package cz.ctu.ieap.pixnet.kathrine_commons.model

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class ValuePayload {

    private val name: String
    private var stringValue: String? = null
    private var intValue: Int? = null
    private var doubleValue: Double? = null
    private var boolValue: Boolean? = null

    constructor(name: String, stringValue: String) {
        this.name = name
        this.stringValue = stringValue
    }

    constructor(name: String, intValue: Int?) {
        this.name = name
        this.intValue = intValue
    }

    constructor(name: String, doubleValue: Double?) {
        this.name = name
        this.doubleValue = doubleValue
    }

    constructor(name: String, boolValue: Boolean?) {
        this.name = name
        this.boolValue = boolValue
    }

    override fun toString(): String {
        var value = ""
        when {
            stringValue != null -> value = stringValue!!
            intValue != null -> value = intValue.toString()
            doubleValue != null -> value = doubleValue.toString()
            boolValue != null -> value = boolValue.toString()
        }
        return "$name: $value"
    }
}
