package cz.ctu.ieap.pixnet.kathrine_commons.model

import cz.ctu.ieap.pixnet.commons.model.AbstractDataFrame

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class AcqConfigurationFrame(val acqMode: DetectorConfig.AcqMode,
                            val acqStartTimestamp: Long,
                            val chipID: String,
                            val readOutMode: DetectorConfig.ReadOutMode,
                            val bias: Float,
                            val temperatureSensor: Float,
                            val temperatureReadout: Float
) : AbstractDataFrame(System.currentTimeMillis())
