package cz.ctu.ieap.pixnet.kathrine_commons

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
abstract class DetectorAbstractConfiguration
