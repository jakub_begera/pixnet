package cz.ctu.ieap.pixnet.kathrine_commons.model

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
enum class DataFrameHeaders private constructor(val id: Int) {

    /**
     * This data frame indicates the readout starts to measure new frame. During frame-based readout mode the readout
     * device sends this frame before each frame. If data-driven mode is activated, the frame is sent only at the
     * beginning of measurement (acquisition).
     *
     *
     * 47..44 (Header) 0x7
     * 43..0 0x00000000000 (Data)
     */
    NEW_FRAME_ESTABLISHED(0x7),

    /**
     * Measured data
     */

    PIXEL_MEASUREMENT_DATA(0x4),
    /**
     * This data frames sends time offset for timestamp of pixels. The final timestamp (ToA) of hit is expressed as
     * sum of this offset multiplied by constant of 16384 and ToA value. This frame is sent when pixel hit needs the
     * new value of pixel offset. Also each UDP datagram with pixel data begins by this frame.
     * (only for data-driven mode)
     *
     *
     * 47..44 0x5 (Header)
     * 43..32 0x000
     * 31..0 Time Offset (Data)
     */
    PIXEL_TIMESTAMP_OFFSET(0x5),

    /**
     * This data frame indicates the end of frame. During frame-based readout mode the readout device sends this frame
     * after each frame. If data-driven mode is activated, the frame is sent only at the end of the whole measurement
     * (acquisition). The frame also tells us how many pixel data frames were sent to client.
     *
     *
     * 47..44 0xC (Header)
     * 43..0 Number of Sent Pixels (Data)
     */
    CURRENT_FRAME_FINISHED(0xC),

    /**
     * These frames send LSB part of timestamp of start frame according to internal counter in Timepix3.
     *
     *
     * 47..44 0x8 (Header)
     * 43..32 0x000
     * 31..0 Start of Frame Timestamp LSB (Data)
     */
    START_OF_FRAME_TIMESTAMP_LSB(0x8),

    /**
     * These frames send MSB part of timestamp of start frame according to internal counter in Timepix3.
     *
     *
     * 47..44 0x8 (Header)
     * 43..32 0x000
     * 31..0 Start of Frame Timestamp MSB (Data)
     */
    START_OF_FRAME_TIMESTAMP_MSB(0x9),

    /**
     * These frames send LSB part of timestamp of end frame according to internal counter in Timepix3.
     *
     *
     * 47..44 0x8 (Header)
     * 43..32 0x000
     * 31..0 End of Frame Timestamp LSB (Data)
     */
    END_OF_FRAME_TIMESTAMP_LSB(0xA),

    /**
     * These frames send MSB part of timestamp of end frame according to internal counter in Timepix3.
     *
     *
     * 47..44 0x8 (Header)
     * 43..32 0x000
     * 31..0 End of Frame Timestamp MSB (Data)
     */
    END_OF_FRAME_TIMESTAMP_MSB(0xB),

    /**
     * This vector indicates the number of lost pixels during measurement (within the current frame). “Lost pixels”
     * stands for how many hit pixels were discarded due to full internal buffers in the readout device. Pixel loss
     * occurs when hit rate of detector is higher than Ethernet internet throughput rate.
     *
     *
     * 47..44 0xD (Header)
     * 43..0 Number of Lost Pixels (Data)
     */
    NUMBER_OF_LOST_PIXELS(0xD),

    /**
     * This message tells us that measurement was aborted before defined time limit because of user request
     * (Acquisition Stop Command).
     *
     *
     * 47..44 0xE (Header)
     * 43..0 0x00000000000 (Data)
     */
    MEASUREMENT_ABORTED_NOTICE(0xE);


    companion object {

        fun getById(id: Int): DataFrameHeaders? {
            for (c in DataFrameHeaders.values()) {
                if (c.id == id) return c
            }
            return null
        }
    }
}
