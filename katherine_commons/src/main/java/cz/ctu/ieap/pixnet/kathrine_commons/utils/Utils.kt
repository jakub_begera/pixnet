package cz.ctu.ieap.pixnet.kathrine_commons.utils

import java.text.SimpleDateFormat
import java.util.*

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
object Utils {

    val dateFormatVerbose: SimpleDateFormat
        get() {
            val dateFormat = SimpleDateFormat("dd.MM.yyyy HH:mm:ss.SSS z")
            dateFormat.timeZone = TimeZone.getTimeZone("UTC")
            return dateFormat
        }

}
