package cz.ctu.ieap.pixnet.kathrine_commons.utils

import java.util.*

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class LsbMsbPacketMatcher<Key, Value> {

    private val lsbSet = HashMap<Key, Value>()
    private val msbSet = HashMap<Key, Value>()

    /**
     * Puts LSB into structure.
     * Check if matching MSB is in the structure already - if yes [LSB, MSB] will be returned.
     *
     * @param keys
     * @param lsb
     * @return
     */
    fun putLsbAndCheck(keys: Key, lsb: Value) = if (msbSet.containsKey(keys)) {
        val msb = msbSet[keys]
        msbSet.remove(keys)
        Pair(lsb, msb!!)
    } else {
        lsbSet[keys] = lsb
        null
    }

    /**
     * Puts MSB into structure.
     * Check if matching LSB is in the structure already - if yes [LSB, MSB] will be returned.
     *
     * @param keys
     * @param msb
     * @return
     */
    fun putMsbAndCheck(keys: Key, msb: Value) = if (lsbSet.containsKey(keys)) {
        val lsb = lsbSet[keys]
        lsbSet.remove(keys)
        Pair(lsb!!, msb)
    } else {
        msbSet[keys] = msb
        null
    }


}
