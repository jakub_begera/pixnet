package cz.ctu.ieap.pixnet.kathrine_commons.model

import cz.ctu.ieap.pixnet.commons.exceptions.DetectorException
import cz.ctu.ieap.pixnet.kathrine_commons.DetectorAbstractConfiguration
import org.yaml.snakeyaml.Yaml
import org.yaml.snakeyaml.constructor.CustomClassLoaderConstructor

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class DetectorConfig : DetectorAbstractConfiguration() {

    // Connection
    var name: String? = null
    var host: String? = null
    var portCommands: Int = 0
    var portData: Int = 0
    var portCommandsToListen: Int? = null
    var portDataToListen: Int? = null

    var outputDir: String? = null

    // acq config
    /**
     * Acquisition time in 10 ns
     */
    var acqTime: Long? = null
    var bias: Float? = null
    var acqMode: AcqMode? = null
    var numberOfFrames: Int? = null
    var triggerEnabled: Boolean? = null
    var triggerConfig: TriggerConfig? = null
    var readoutMode: ReadOutMode? = null
    var pixelConfig: String? = null

    val biasOrThrow: Float
        @Throws(DetectorException::class)
        get() = bias?.let { return it } ?: throw DetectorException("Bias not set")

    val acqModeOrThrow: AcqMode
        @Throws(DetectorException::class)
        get() = acqMode?.let { return it } ?: throw DetectorException("Acq mode not set")

    val readoutModeOrThrow: ReadOutMode
        @Throws(DetectorException::class)
        get() = readoutMode?.let { return it } ?: throw DetectorException("Readout mode not set")

    enum class AcqMode private constructor(val payloadOlder: String, val fastVCOEnabled: Boolean, val mode: String, val modeID: Int) {
        /**
         * Payload: ToA ToT FastToA
         */
        Fast_VCO_ON_TOA_TOT("ToA\tToT", true, "ToA & ToT with FastToA", 0),
        /**
         * Payload: ToA - FastToA
         */
        Fast_VCO_ON_TOA("ToA", true, "Only ToA with FastToA", 1),
        /**
         * Payload: iTOT EventCount -
         */
        Fast_VCO_ON_EventCount_iToT("iTOT\tEventCount", true, "Event Count and iToT with fast VCO", 2),
        /**
         * Payload: ToA ToT HitCounter
         */
        Fast_VCO_OFF_TOA_TOT("ToA\tToT\tHitCounter", false, "ToA & ToT", 0),
        /**
         * Payload: ToA - HitCounter
         */
        Fast_VCO_OFF_TOA("ToA\tHitCounter", false, "Only ToA", 1),
        /**
         * Payload: iTOT EventCount HitCounter
         */
        Fast_VCO_OFF_EventCount_iToT("iTOT\tEventCount\tHitCounter", false, "Event Count and iToT", 2);


        companion object {

            @Throws(DetectorException::class)
            fun find(modeID: Int, fastVCOEnabled: Boolean): AcqMode? {
                if (fastVCOEnabled) {
                    when (modeID) {
                        0 -> return Fast_VCO_ON_TOA_TOT
                        1 -> return Fast_VCO_ON_TOA
                        2 -> return Fast_VCO_ON_EventCount_iToT
                    }
                } else {
                    when (modeID) {
                        0 -> return Fast_VCO_OFF_TOA_TOT
                        1 -> return Fast_VCO_OFF_TOA
                        2 -> return Fast_VCO_OFF_EventCount_iToT
                    }
                }
                return null
            }
        }
    }

    enum class ReadOutMode private constructor(val mode: String, val value: Int) {
        FRAME("Frame based", 0),
        DATA_DRIVEN("Data driven", 1);


        companion object {

            fun byValue(value: Int): ReadOutMode? {
                for (it in values()) {
                    if (it.value == value) return it
                }
                return null
            }
        }
    }

    class TriggerConfig {

        var startMode: StartMode? = null
        var finishMode: FinishMode? = null
        var acqStartTriggerChannel: Int? = 0
        var acqEndTriggerChannel: Int? = 0
        var edgeSetupStart: EdgeSetup? = null
        var edgeSetupFinish: EdgeSetup? = null
        var isDelayedStart: Boolean? = false

        constructor() {}

        constructor(startMode: StartMode?,
                    finishMode: FinishMode?,
                    acqStartTriggerChannel: Int?,
                    acqEndTriggerChannel: Int?,
                    edgeSetupStart: EdgeSetup?,
                    edgeSetupFinish: EdgeSetup?,
                    delayedStart: Boolean
        ) {
            this.startMode = startMode
            this.finishMode = finishMode
            this.acqStartTriggerChannel = acqStartTriggerChannel
            this.acqEndTriggerChannel = acqEndTriggerChannel
            this.edgeSetupStart = edgeSetupStart
            this.edgeSetupFinish = edgeSetupFinish
            this.isDelayedStart = delayedStart
        }

        enum class EdgeSetup private constructor(val value: Int) {
            RISING_EDGE(0),
            FALLING_EDGE(1);


            companion object {

                fun byValue(value: Int): EdgeSetup? {
                    for (it in values()) {
                        if (it.value == value) return it
                    }
                    return null
                }
            }
        }

        enum class StartMode private constructor(val value: Int) {

            IMMEDIATELY(0),
            BY_TRIGGER(1);


            companion object {

                fun byValue(value: Int): StartMode? {
                    for (it in values()) {
                        if (it.value == value) return it
                    }
                    return null
                }
            }
        }

        enum class FinishMode private constructor(val value: Int) {

            BY_TIMER(0),
            BY_TRIGGER(1);


            companion object {

                fun byValue(value: Int): FinishMode? {
                    for (it in values()) {
                        if (it.value == value) return it
                    }
                    return null
                }
            }
        }
    }

    companion object {

        fun fromYaml(config: String, classLoader: ClassLoader?): DetectorConfig {
            val yaml = if (classLoader != null) {
                Yaml(CustomClassLoaderConstructor(classLoader))
            } else {
                Yaml()
            }
            return yaml.loadAs(config, DetectorConfig::class.java)
        }
    }
}
