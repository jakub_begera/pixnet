package cz.ctu.ieap.pixnet.kathrine_commons.model

import cz.ctu.ieap.pixnet.commons.model.AbstractDataFrame

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class ControlFrame(val type: Type) : AbstractDataFrame(System.currentTimeMillis()) {
    var payload: Any? = null

    constructor(type: Type, payload: Any) : this(type) {
        this.payload = payload
    }

    constructor(type: Type, string: String, vararg args: Any) : this(type) {
        this.payload = String.format(string, *args)
    }

    override fun toString(): String {
        return when (type) {
            ControlFrame.Type.PERSIST_VALUE -> payload!!.toString()
            ControlFrame.Type.MEASUREMENT_STARTED -> "Measurement started"
            ControlFrame.Type.MEASUREMENT_FINISHED -> "Measurement finished"
        }
    }

    enum class Type {
        MEASUREMENT_STARTED,
        MEASUREMENT_FINISHED,
        PERSIST_VALUE
    }
}
