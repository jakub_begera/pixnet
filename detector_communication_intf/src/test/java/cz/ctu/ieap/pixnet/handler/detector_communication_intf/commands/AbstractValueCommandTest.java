package cz.ctu.ieap.pixnet.handler.detector_communication_intf.commands;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.commands.value.AbstractValueCommand;
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.commands.value.ValueCommand;
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.commands.value.ValueCommandGroup;
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.model.ValueUnit;
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.model.value_model.BooleanValueModel;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
public class AbstractValueCommandTest {

    @Test
    public void destSerializationDeserialization() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();

        ValueCommand valueCommand = new ValueCommand(11, "nameFull", ValueUnit.NANO_SECOND,
                ValueCommand.AccessType.SETTER_AND_GETTER, new BooleanValueModel());
        ValueCommandGroup group = new ValueCommandGroup("group nameFull", valueCommand);

        String valueJSON = objectMapper.writeValueAsString(valueCommand);
        String groupJSON = objectMapper.writeValueAsString(group);

        AbstractValueCommand valueAbstract = objectMapper.readValue(valueJSON, AbstractValueCommand.class);
        AbstractValueCommand groupAbstract = objectMapper.readValue(groupJSON, AbstractValueCommand.class);

        assertNotNull(valueAbstract);
        assertNotNull(groupAbstract);


    }

}