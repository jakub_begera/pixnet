package cz.ctu.ieap.pixnet.handler.detector_communication_intf.model

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
enum class ValueUnit private constructor(val nameFull: String, val nameShort: String) {
    DIMENSIONLESS("dimensionless", "-"),
    NANO_SECOND("nano second", "ns"),
    SECOND("second", "s"),
    VOLT("Volt", "V"),
    STRING("text", ""),
    CELSIUS("Celsius", "°C")
}