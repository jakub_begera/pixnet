package cz.ctu.ieap.pixnet.handler.detector_communication_intf.commands.execution

import cz.ctu.ieap.pixnet.handler.detector_communication_intf.model.value_model.ValueModelVerbose

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class ExecutionCommand : AbstractExecutionCommand {

    var id: Int = 0
    var name: String? = null
    var inputValuesModel: Array<ValueModelVerbose>? = null
    var outputValuesModel: Array<ValueModelVerbose>? = null

    constructor(id: Int, name: String, inputValuesModel: Array<ValueModelVerbose>?, outputValuesModel: Array<ValueModelVerbose>?) {
        this.id = id
        this.name = name
        this.inputValuesModel = inputValuesModel
        this.outputValuesModel = outputValuesModel
    }
}
