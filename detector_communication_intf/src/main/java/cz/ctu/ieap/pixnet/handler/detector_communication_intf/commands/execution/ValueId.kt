package cz.ctu.ieap.pixnet.handler.detector_communication_intf.commands.execution

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
enum class ValueId {
    temperature,
    frames_count,
    fast_vco_en,
    acq_mode,
    hw_type,
    hw_revision,
    hw_serial_number,
    fw_version,
    readout_hw_command_id,
    digital_test_result,
    start_mode,
    hw_start_trigger_channel,
    edge_setup_start,
    delayed_start_by_hw_event,
    acq_finish,
    hw_finish_trigger_channel,
    edge_setup_finish,
    readout_mode
}
