package cz.ctu.ieap.pixnet.handler.detector_communication_intf.commands.value

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class ValueCommandGroup : AbstractValueCommand {

    var name: String? = null
    var valueCommands: List<ValueCommand> = listOf()

    constructor() {}

    constructor(name: String, valueCommands: List<ValueCommand>) {
        this.name = name
        this.valueCommands = valueCommands
    }

    constructor(name: String, vararg valueCommands: ValueCommand) {
        this.name = name
        this.valueCommands = listOf(*valueCommands)
    }
}
