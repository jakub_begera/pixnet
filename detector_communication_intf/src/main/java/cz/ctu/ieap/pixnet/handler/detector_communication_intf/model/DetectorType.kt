package cz.ctu.ieap.pixnet.handler.detector_communication_intf.model

enum class DetectorType private constructor(val nameFull: String) {
    MEDIPIX("medipix"),
    TIMEPIX("timepix"),
    TIMEPIX3("timepix3"),
    OTHER("other")
}
