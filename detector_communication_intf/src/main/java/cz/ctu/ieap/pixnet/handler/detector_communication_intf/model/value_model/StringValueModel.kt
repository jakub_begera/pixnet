package cz.ctu.ieap.pixnet.handler.detector_communication_intf.model.value_model

import com.fasterxml.jackson.annotation.JsonInclude

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class StringValueModel : ValueModel {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    var discrete: Map<String, String>? = null

    override val isDiscrete: Boolean
        get() = discrete != null

    constructor() {}

    constructor(discrete: Map<String, String>) {
        this.discrete = discrete
    }
}
