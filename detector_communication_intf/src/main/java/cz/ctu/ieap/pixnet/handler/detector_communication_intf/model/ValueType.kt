package cz.ctu.ieap.pixnet.handler.detector_communication_intf.model

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
enum class ValueType {
    INT,
    LONG,
    FLOAT,
    DOUBLE,
    BOOLEAN,
    STRING
}
