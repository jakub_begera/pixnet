package cz.ctu.ieap.pixnet.handler.detector_communication_intf.utils

import cz.ctu.ieap.pixnet.commons.exceptions.DetectorException
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.model.ValuePayload

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
object ValueUtils {

    @Throws(DetectorException::class)
    fun getIntValueById(input: Map<String, ValuePayload>, id: String): Int {
        val valuePayload = input[id]
        if (valuePayload?.intValue == null)
            throw DetectorException("No input value with id %s found.", id)
        return valuePayload.intValue!!
    }

    @Throws(DetectorException::class)
    fun getLongValueById(input: Map<String, ValuePayload>, id: String): Long {
        val valuePayload = input[id]
        if (valuePayload?.longValue == null)
            throw DetectorException("No input value with id %s found.", id)
        return valuePayload.longValue!!
    }

    @Throws(DetectorException::class)
    fun getFloatValueById(input: Map<String, ValuePayload>, id: String): Float {
        val valuePayload = input[id]
        if (valuePayload?.floatValue == null)
            throw DetectorException("No input value with id %s found.", id)
        return valuePayload.floatValue!!
    }

    @Throws(DetectorException::class)
    fun getDobleValueById(input: Map<String, ValuePayload>, id: String): Double {
        val valuePayload = input[id]
        if (valuePayload?.doubleValue == null)
            throw DetectorException("No input value with id %s found.", id)
        return valuePayload.doubleValue!!
    }

    @Throws(DetectorException::class)
    fun getBooleanValueById(input: Map<String, ValuePayload>, id: String): Boolean {
        val valuePayload = input[id]
        if (valuePayload?.booleanValue == null)
            throw DetectorException("No input value with id %s found.", id)
        return valuePayload.booleanValue!!
    }

    @Throws(DetectorException::class)
    fun getStringValueById(input: Map<String, ValuePayload>, id: String): String? {
        val valuePayload = input[id]
        if (valuePayload?.stringValue == null)
            throw DetectorException("No input value with id %s found.", id)
        return valuePayload.stringValue
    }

}
