package cz.ctu.ieap.pixnet.handler.detector_communication_intf.model.value_model

import com.fasterxml.jackson.annotation.JsonInclude

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class IntValueModel : ValueModel {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    var min: Int? = null
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var max: Int? = null
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var discrete: Map<String, Int>? = null

    override val isDiscrete: Boolean
        get() = discrete != null

    constructor() {
        min = Integer.MIN_VALUE
        max = Integer.MAX_VALUE
        discrete = null
    }

    constructor(min: Int, max: Int) {
        this.min = min
        this.max = max
        discrete = null
    }

    constructor(discrete: Map<String, Int>) {
        this.discrete = discrete
        min = null
        max = null
    }
}
