package cz.ctu.ieap.pixnet.handler.detector_communication_intf.commands.execution

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class ExecutionCommandGroup : AbstractExecutionCommand {

    var name: String? = null
    var executionCommands: List<ExecutionCommand> = listOf()

    constructor() {}

    constructor(name: String, executionCommands: List<ExecutionCommand>) {
        this.name = name
        this.executionCommands = executionCommands
    }

    constructor(name: String, vararg executionCommands: ExecutionCommand) {
        this.name = name
        this.executionCommands = listOf(*executionCommands)
    }
}
