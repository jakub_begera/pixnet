package cz.ctu.ieap.pixnet.handler.detector_communication_intf.model.value_model

import com.fasterxml.jackson.annotation.JsonInclude
import java.util.*

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class BooleanValueModel : ValueModel {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    var discrete: Map<String, Boolean>? = null
        private set

    override val isDiscrete: Boolean
        get() = discrete != null

    constructor() {}

    constructor(discrete: Map<String, Boolean>) {
        this.discrete = discrete
    }

    fun setDiscrete(discrete: HashMap<String, Boolean>) {
        this.discrete = discrete
    }
}
