package cz.ctu.ieap.pixnet.handler.detector_communication_intf.model

import com.fasterxml.jackson.annotation.JsonInclude

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class ValuePayload {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    var intValue: Int? = null
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var longValue: Long? = null
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var floatValue: Float? = null
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var doubleValue: Double? = null
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var booleanValue: Boolean? = null
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var stringValue: String? = null

    val type: ValueType?
        get() {
            if (intValue != null) return ValueType.INT
            if (longValue != null) return ValueType.LONG
            if (floatValue != null) return ValueType.FLOAT
            if (doubleValue != null) return ValueType.DOUBLE
            if (booleanValue != null) return ValueType.BOOLEAN
            return if (stringValue != null) ValueType.STRING else null
        }

    constructor() {}

    constructor(intValue: Int?) {
        this.intValue = intValue
    }

    constructor(longValue: Long?) {
        this.longValue = longValue
    }

    constructor(floatValue: Float?) {
        this.floatValue = floatValue
    }

    constructor(doubleValue: Double?) {
        this.doubleValue = doubleValue
    }

    constructor(booleanValue: Boolean?) {
        this.booleanValue = booleanValue
    }

    constructor(stringValue: String) {
        this.stringValue = stringValue
    }

    override fun toString(): String {
        val sb = StringBuilder()
        intValue?.let { sb.append(it) }
        longValue?.let { sb.append(it) }
        floatValue?.let { sb.append(it) }
        doubleValue?.let { sb.append(it) }
        booleanValue?.let { sb.append(it) }
        stringValue?.let { sb.append(it) }
        return sb.toString()
    }
}
