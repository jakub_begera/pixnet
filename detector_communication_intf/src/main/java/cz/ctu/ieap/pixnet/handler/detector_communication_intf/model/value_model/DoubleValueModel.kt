package cz.ctu.ieap.pixnet.handler.detector_communication_intf.model.value_model

import com.fasterxml.jackson.annotation.JsonInclude

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class DoubleValueModel : ValueModel {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    var min: Double? = null
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var max: Double? = null
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var discrete: Map<String, Double>? = null

    override val isDiscrete: Boolean
        get() = discrete != null

    constructor() {
        min = java.lang.Double.MIN_VALUE
        max = java.lang.Double.MAX_VALUE
        discrete = null
    }

    constructor(min: Double, max: Double) {
        this.min = min
        this.max = max
        discrete = null
    }

    constructor(discrete: Map<String, Double>) {
        this.discrete = discrete
        min = null
        max = null
    }
}
