package cz.ctu.ieap.pixnet.handler.detector_communication_intf.model.value_model

import com.fasterxml.jackson.annotation.JsonInclude

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class FloatValueModel : ValueModel {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    var min: Float? = null
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var max: Float? = null
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var discrete: Map<String, Float>? = null

    override val isDiscrete: Boolean
        get() = discrete != null

    constructor() {
        min = java.lang.Float.MIN_VALUE
        max = java.lang.Float.MAX_VALUE
        discrete = null
    }

    constructor(min: Float, max: Float) {
        this.min = min
        this.max = max
        discrete = null
    }

    constructor(discrete: Map<String, Float>) {
        this.discrete = discrete
        min = null
        max = null
    }
}
