package cz.ctu.ieap.pixnet.handler.detector_communication_intf.model.value_model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
@JsonSubTypes(
        JsonSubTypes.Type(value = IntValueModel::class),
        JsonSubTypes.Type(value = LongValueModel::class),
        JsonSubTypes.Type(value = FloatValueModel::class),
        JsonSubTypes.Type(value = DoubleValueModel::class),
        JsonSubTypes.Type(value = BooleanValueModel::class),
        JsonSubTypes.Type(value = StringValueModel::class)
)
abstract class ValueModel {

    abstract val isDiscrete: Boolean

}
