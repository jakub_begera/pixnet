package cz.ctu.ieap.pixnet.handler.detector_communication_intf;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.ctu.ieap.pixnet.commons.exceptions.DetectorException;
import cz.ctu.ieap.pixnet.commons.model.AbstractDataFrame;
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.commands.execution.AbstractExecutionCommand;
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.commands.value.AbstractValueCommand;
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.commands.value.ValueCommand;
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.model.DetectorType;
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.model.ValuePayload;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
public interface DetectorComm {

    /**
     * Return type of the detector (Timepix, Medipix, ...).
     */
    public DetectorType getDetectorType();

    /**
     * Return nameFull of readout device.
     */
    public String getReadoutName();

    /**
     * Return number of Timepix/Medipix sensors.
     */
    public int getSensorsCount();

    /**
     * Return number of pixels in one row of the detector.
     */
    public int getDetectorWidth();

    /**
     * Return number of pixels in one column of the detector.
     */
    public int getDetectorHeight();

    @JsonIgnore
    @Nullable
    public String getDetectorConfig();

    /**
     *
     * @param config String configuration of the detector (YAML, JSON, ...)
     */
    @JsonIgnore
    public void setDetectorConfig(String config);

    /**
     * Returns list of supported ${@link ValueCommand} of the detector.
     */
    public List<AbstractValueCommand> getSupportedValueCommands();

    public List<AbstractExecutionCommand> getSupportedExecutionCommands();

    /**
     * Connect the detector.
     */
    @JsonIgnore
    public boolean connect() throws DetectorException;

    /**
     * Disconnect the detector.
     */
    @JsonIgnore
    public boolean disconnect() throws DetectorException;

    /**
     * Check connection of the detector.
     * @return true if detector is connected.
     */
    @JsonIgnore
    public boolean isConnected();

    /**
     * Set a value to the detector
     * @param commandID ID of command which will be executed
     * @param payload value to set
     * @return true if success
     * @throws DetectorException in case of IOException, unknown commandID, etc.
     */
    public void executeSetValueCommand(int commandID, ValuePayload payload) throws DetectorException;

    /**
     * Get a value from the detector.
     * @param commandID ID of command which will be executed
     * @return desired value
     * @throws DetectorException in case of IOException, unknown commandID, etc.
     */
    public ValuePayload executeGetValueCommand(int commandID) throws DetectorException;

    public Map<String, ValuePayload> executeExecutionCommand(int commandID, Map<String, ValuePayload> input) throws DetectorException;

    public List<String> getAcceptedFilesKeys();

    public void uploadFile(String fileKey, byte[] file) throws DetectorException;

    public BlockingQueue<AbstractDataFrame> getDataFrameQueue();

    public void setCallback(Callback callback);

    public interface Callback {
        ClassLoader getClassLoader();
    }

}
