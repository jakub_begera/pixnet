package cz.ctu.ieap.pixnet.handler.detector_communication_intf.model.value_model

import com.fasterxml.jackson.annotation.JsonInclude

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class LongValueModel : ValueModel {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    var min: Long? = null
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var max: Long? = null
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var discrete: Map<String, Long>? = null

    override val isDiscrete: Boolean
        get() = discrete != null

    constructor() {
        min = java.lang.Long.MIN_VALUE
        max = java.lang.Long.MAX_VALUE
        discrete = null
    }

    constructor(min: Long, max: Long) {
        this.min = min
        this.max = max
        discrete = null
    }

    constructor(discrete: Map<String, Long>) {
        this.discrete = discrete
        min = null
        max = null
    }
}
