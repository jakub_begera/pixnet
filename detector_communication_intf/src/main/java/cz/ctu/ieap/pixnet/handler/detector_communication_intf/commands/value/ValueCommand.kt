package cz.ctu.ieap.pixnet.handler.detector_communication_intf.commands.value

import cz.ctu.ieap.pixnet.handler.detector_communication_intf.model.ValueUnit
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.model.value_model.ValueModel
import org.slf4j.LoggerFactory

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class ValueCommand : AbstractValueCommand {

    private val logger = LoggerFactory.getLogger(this.javaClass)

    var id: Int = 0
    private var name: String? = null
    var valueUnit: ValueUnit? = null
    var accessType: AccessType? = null
    var inputValueModel: ValueModel? = null

    constructor(id: Int, name: String, valueUnit: ValueUnit, accessType: AccessType, inputValueModel: ValueModel) {
        this.id = id
        this.name = name
        this.valueUnit = valueUnit
        this.accessType = accessType
        this.inputValueModel = inputValueModel
    }

    constructor() {}

    fun setName(name: String) {
        this.name = name
    }

    enum class AccessType {
        SETTER,
        GETTER,
        SETTER_AND_GETTER,
        NONE;

        val allowed: Boolean
            get() = this == GETTER || this == SETTER_AND_GETTER

        fun setAllowed(): Boolean {
            return this == SETTER || this == SETTER_AND_GETTER
        }
    }
}
