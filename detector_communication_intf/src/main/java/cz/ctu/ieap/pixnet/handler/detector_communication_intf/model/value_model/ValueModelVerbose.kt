package cz.ctu.ieap.pixnet.handler.detector_communication_intf.model.value_model

import cz.ctu.ieap.pixnet.handler.detector_communication_intf.model.ValueUnit

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class ValueModelVerbose {

    var valueName: String? = null
    var valueID: String? = null
    var valueModel: ValueModel? = null
    var valueUnit: ValueUnit? = null

    constructor() {}

    constructor(valueName: String, valueID: String, valueModel: ValueModel, valueUnit: ValueUnit) {
        this.valueName = valueName
        this.valueID = valueID
        this.valueModel = valueModel
        this.valueUnit = valueUnit
    }
}
