package cz.ctu.ieap.pixnet.master

import cz.ctu.ieap.pixnet.master.model.config.ExternalConfig
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.yaml.snakeyaml.Yaml
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.util.*

@SpringBootApplication
class Application {

    companion object {

        const val APP_VERSION = "0.0.0.1" //TODO get from build config

        @Throws(FileNotFoundException::class)
        @JvmStatic
        fun main(args: Array<String>) {
            var config: ExternalConfig? = null
            for (arg in args) {
                if (arg.startsWith("masterConfig=")) {
                    val yaml = Yaml()
                    val configPath = arg.split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1]
                    config = yaml.loadAs(FileInputStream(configPath), ExternalConfig::class.java)
                    System.getProperties()["externalConfigPath"] = configPath
                }
            }
            if (config == null) {
                System.err.println("Config not initialized. Did you forgot handlerConfig=PATH... argument?")
                System.exit(1)
            }
            val props = HashMap<String, Any>()
            props["server.port"] = config!!.portToListen

            SpringApplicationBuilder()
                    .sources(Application::class.java)
                    .properties(props)
                    .run(*args)

        }
    }
}
