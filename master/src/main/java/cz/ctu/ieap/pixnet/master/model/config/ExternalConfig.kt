package cz.ctu.ieap.pixnet.master.model.config

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class ExternalConfig {

    var portToListen: Int = 0
}
