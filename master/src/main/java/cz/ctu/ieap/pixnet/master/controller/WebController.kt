package cz.ctu.ieap.pixnet.master.controller

import cz.ctu.ieap.pixnet.master.Application.Companion.APP_VERSION
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
@Controller
class WebController {

    @RequestMapping("/detectors")
    fun detectors(model: Model): String {
        addHeaderAndFooterAttributes(model, "detectors")
        return "detectors"
    }

    @RequestMapping("/about")
    fun about(model: Model): String {
        addHeaderAndFooterAttributes(model, "about")
        return "about"
    }

    private fun addHeaderAndFooterAttributes(model: Model, module: String) {
        model.addAttribute("module", module)
        model.addAttribute("appVersion", APP_VERSION)
    }

}
