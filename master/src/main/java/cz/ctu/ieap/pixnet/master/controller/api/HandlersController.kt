package cz.ctu.ieap.pixnet.master.controller.api

import cz.ctu.ieap.pixnet.commons.utils.EasyLogger
import cz.ctu.ieap.pixnet.master.model.entity.Handler
import cz.ctu.ieap.pixnet.master.repository.HandlerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
@RestController
@RequestMapping("/api/handlers")
class HandlersController {

    private val logger = EasyLogger(this.javaClass)

    @Autowired
    private lateinit var handlerRepository: HandlerRepository

    val all: ResponseEntity<*>
        @GetMapping("/getAll")
        get() {
            val all = handlerRepository.findAll()
            return ResponseEntity.ok(all)
        }


    @PostMapping("/add")
    fun addHandler(@RequestBody request: Handler): ResponseEntity<*> {
        handlerRepository.save(request)
        return ResponseEntity.ok().build<Any>()
    }

    @DeleteMapping("/deleteAll")
    fun removeAll(): ResponseEntity<*> {
        handlerRepository.deleteAll()
        return ResponseEntity.ok().build<Any>()
    }

    @DeleteMapping("/deleteById")
    fun removeById(@RequestParam("id") id: Int): ResponseEntity<*> {
        handlerRepository.deleteById(id)
        return ResponseEntity.ok().build<Any>()
    }


}
