package cz.ctu.ieap.pixnet.handler.detector_communication_katherine.utils;

import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.assertArrayEquals;

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
public class KathrineUtilsTest {

    @Test
    public void testDecodeEncodeMatrixConfig() {

        byte[] bytes = new byte[256 * 256];
        new Random().nextBytes(bytes);

        String encodeMatrixConfig = KathrineUtils.INSTANCE.encodeMatrixConfig(bytes);
        byte[] decodeMatrixConfig = KathrineUtils.INSTANCE.decodeMatrixConfig(encodeMatrixConfig);

        assertArrayEquals(bytes, decodeMatrixConfig);

    }
}