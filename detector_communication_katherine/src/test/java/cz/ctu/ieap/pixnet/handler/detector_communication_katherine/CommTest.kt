package cz.ctu.ieap.pixnet.handler.detector_communication_katherine

import cz.ctu.ieap.pixnet.handler.detector_communication_katherine.model.KatherineValueCommands
import cz.ctu.ieap.pixnet.handler.detector_communication_katherine.utils.PacketBuilder
import org.junit.Test

import java.io.IOException
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class CommTest {

    @Test
    @Throws(IOException::class)
    fun echoTest() {
        val buffer = PacketBuilder.builder()
                .commandID(KatherineValueCommands.CHIP_ID.getterCommandId)
                .build()
        val address = InetAddress.getByName("192.168.1.122")

        val socket = DatagramSocket(1555)
        val packetConfig = DatagramPacket(buffer, 8, address, 1555)
        socket.send(packetConfig)


        socket.receiveBufferSize = 8
        socket.soTimeout = 10000
        val buff = ByteArray(8)
        val packet = DatagramPacket(buff, buff.size)

        socket.receive(packet)
        socket.close()
    }

}
