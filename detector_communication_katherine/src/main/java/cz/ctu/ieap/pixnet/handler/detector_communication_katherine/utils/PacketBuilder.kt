package cz.ctu.ieap.pixnet.handler.detector_communication_katherine.utils

import cz.ctu.ieap.pixnet.commons.utils.ByteUtils
import java.util.*

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class PacketBuilder private constructor() {

    private val buff: ByteArray = ByteArray(8)

    fun build(): ByteArray {
        return buff
    }

    fun commandID(id: Int): PacketBuilder {
        assert(id in 0..65024)
        val bytes = ByteUtils.intToBytes(id)
        buff[7] = bytes[2]
        buff[6] = bytes[3]
        return this
    }

    fun putInt(value: Int): PacketBuilder {
        val bytes = ByteUtils.intToBytes(value)
        putBytes(bytes)
        return this
    }

    fun putLongLSB(value: Long): PacketBuilder {
        val bytes = ByteUtils.longToBytes(value)
        putBytes(Arrays.copyOfRange(bytes, 4, 8))
        return this
    }

    fun putLongMSB(value: Long): PacketBuilder {
        val bytes = ByteUtils.longToBytes(value)
        putBytes(Arrays.copyOfRange(bytes, 0, 4))
        return this
    }

    fun putFloat(value: Float): PacketBuilder {
        val bytes = ByteUtils.floatToBytes(value)
        putBytes(bytes)
        return this
    }

    fun putByte(index: Int, value: Byte): PacketBuilder {
        buff[buff.size - 1 - index] = value
        return this
    }

    fun putBytes(bytes: ByteArray): PacketBuilder {
        assert(bytes.size <= 6)
        for (i in bytes.indices) {
            buff[i] = bytes[bytes.size - 1 - i]
        }
        return this
    }

    companion object {

        fun builder(): PacketBuilder {
            return PacketBuilder()
        }
    }
}
