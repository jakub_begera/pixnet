package cz.ctu.ieap.pixnet.handler.detector_communication_katherine.model;

import static cz.ctu.ieap.pixnet.handler.detector_communication_katherine.model.DACs.*;
import static cz.ctu.ieap.pixnet.handler.detector_communication_katherine.model.SensorRegisters.*;

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
public enum KatherineValueCommands {

    ACQUISITION_TIME(100, 0x01, 0x0A, 0, 0),
    BIAS(101, 0x02, 0x0C),
    CHIP_ID(102, 0, 0x0B),


    // DACs
    DAC_Ibias_Preamp_ON(Ibias_Preamp_ON),
    DAC_Ibias_Preamp_OFF(Ibias_Preamp_OFF),
    DAC_VPreamp_NCAS(VPreamp_NCAS),
    DAC_Ibias_Ikrum(Ibias_Ikrum),
    DAC_Vfbk(Vfbk),
    DAC_Vthreshold_fine(Vthreshold_fine),
    DAC_Vthreshold_coarse(Vthreshold_coarse),
    DAC_Ibias_DiscS1_ON(Ibias_DiscS1_ON),
    DAC_Ibias_DiscS1_OFF(Ibias_DiscS1_OFF),
    DAC_Ibias_DiscS2_ON(Ibias_DiscS2_ON),
    DAC_Ibias_DiscS2_OFF(Ibias_DiscS2_OFF),
    DAC_Ibias_PixelDAC(Ibias_PixelDAC),
    DAC_Ibias_TPbufferIn(Ibias_TPbufferIn),
    DAC_Ibias_TPbufferOut(Ibias_TPbufferOut),
    DAC_VTP_coarse(VTP_coarse),
    DAC_VTP_fine(VTP_fine),
    DAC_Ibias_CP_PLL(Ibias_CP_PLL),
    DAC_PLL_Vcntrl(PLL_Vcntrl),
    DAC_BandGap_output(BandGap_output),
    DAC_BandGap_Temp(BandGap_Temp),
    DAC_Ibias_dac(Ibias_dac),
    DAC_Ibias_dac_cas(Ibias_dac_cas),
    DAC_VIRTUAL(-1, 0x04, 0x0F),


    // Sensor registers
    SR_TEST_PULSE_PERIOD(TEST_PULSE_PERIOD),
    SR_NUMBER_TEST_PULSES(NUMBER_TEST_PULSES),
    SR_OUT_BLOCK_CONFIG(OUT_BLOCK_CONFIG),
    SR_PLL_CONFIG(PLL_CONFIG),
    SR_GENERAL_CONFIG(GENERAL_CONFIG),
    SR_SLVS_CONFIG(SLVS_CONFIG),
    SR_POWER_PULSING_PATTERN(POWER_PULSING_PATTERN),
    SR_SET_TIMER_15_0(SET_TIMER_15_0),
    SR_SET_TIMER_31_16(SET_TIMER_31_16),
    SR_SET_TIMER_47_32(SET_TIMER_47_32),
    SR_SENSE_DAC_SELECTOR(SENSE_DAC_SELECTOR),
    SR_EXT_DAC_SELECTOR(EXT_DAC_SELECTOR),
    SR_SET_TIMER_VIRTUAL(SET_TIMER_VIRTUAL),
    SR_VIRTUAL(-2, 0x08, 0);

    public final int id;
    public final int setterCommandId;
    public final int getterCommandId;
    public final int setterCommandIdLSB;
    public final int setterCommandIdMSB;
    public final int getterCommandIdLSB;
    public final int getterCommandIdMSB;
    public final boolean hasLsbAndMsb;

    KatherineValueCommands(DACs dac) {
        this.id = 1000 + dac.getInternalID();
        this.setterCommandId = 0x04;
        this.getterCommandId = 0x0F;
        hasLsbAndMsb = false;
        setterCommandIdLSB = 0;
        setterCommandIdMSB = 0;
        getterCommandIdLSB = 0;
        getterCommandIdMSB = 0;
    }

    KatherineValueCommands(SensorRegisters sr) {
        this.id = 2000 + sr.getId();
        this.setterCommandId = 0x08;
        this.getterCommandId = 0;
        hasLsbAndMsb = false;
        setterCommandIdLSB = 0;
        setterCommandIdMSB = 0;
        getterCommandIdLSB = 0;
        getterCommandIdMSB = 0;
    }

    KatherineValueCommands(int id, int setterCommandId, int getterCommandId) {
        this.id = id;
        this.setterCommandId = setterCommandId;
        this.getterCommandId = getterCommandId;
        hasLsbAndMsb = false;
        setterCommandIdLSB = 0;
        setterCommandIdMSB = 0;
        getterCommandIdLSB = 0;
        getterCommandIdMSB = 0;
    }

    KatherineValueCommands(int id, int setterCommandIdLSB, int setterCommandIdMSB, int getterCommandIdLSB, int getterCommandIdMSB) {
        this.id = id;
        this.setterCommandIdLSB = setterCommandIdLSB;
        this.setterCommandIdMSB = setterCommandIdMSB;
        this.getterCommandIdLSB = getterCommandIdLSB;
        this.getterCommandIdMSB = getterCommandIdMSB;
        hasLsbAndMsb = true;
        setterCommandId = 0;
        getterCommandId = 0;
    }

    public static KatherineValueCommands getById(int id) {
        for (KatherineValueCommands c : KatherineValueCommands.values()) {
            if (c.id == id) return c;
        }
        return null;
    }

    public static KatherineValueCommands getByCommunicationId(int id) {
        for (KatherineValueCommands c : KatherineValueCommands.values()) {
            if (c.getterCommandId == id
                    || c.setterCommandId == id
                    || c.setterCommandIdLSB == id
                    || c.setterCommandIdMSB == id
                    || c.getterCommandIdLSB == id
                    || c.getterCommandIdMSB == id
            ) return c;
        }
        return null;
    }
}
