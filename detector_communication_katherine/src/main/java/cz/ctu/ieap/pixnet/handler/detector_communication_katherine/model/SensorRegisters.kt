package cz.ctu.ieap.pixnet.handler.detector_communication_katherine.model

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
enum class SensorRegisters private constructor(id: Int) {

    TEST_PULSE_PERIOD(0),
    NUMBER_TEST_PULSES(1),
    OUT_BLOCK_CONFIG(2),
    PLL_CONFIG(3),
    GENERAL_CONFIG(4),
    SLVS_CONFIG(5),
    POWER_PULSING_PATTERN(6),
    SET_TIMER_15_0(7),
    SET_TIMER_31_16(8),
    SET_TIMER_47_32(9),
    SENSE_DAC_SELECTOR(10),
    EXT_DAC_SELECTOR(11),
    SET_TIMER_VIRTUAL(100);

    val id: Int = 2000 + id

    companion object {

        fun getById(id: Int): SensorRegisters? {
            for (c in SensorRegisters.values()) {
                if (c.id == id) return c
            }
            return null
        }
    }
}
