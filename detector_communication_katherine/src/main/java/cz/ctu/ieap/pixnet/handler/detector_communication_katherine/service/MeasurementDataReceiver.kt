package cz.ctu.ieap.pixnet.handler.detector_communication_katherine.service

import cz.ctu.ieap.pixnet.commons.exceptions.DetectorException
import cz.ctu.ieap.pixnet.commons.model.AbstractDataFrame
import cz.ctu.ieap.pixnet.commons.utils.EasyLogger
import cz.ctu.ieap.pixnet.kathrine_commons.model.DataFrame
import java.io.IOException
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.SocketTimeoutException
import java.util.*
import java.util.concurrent.BlockingQueue
import java.util.concurrent.LinkedBlockingQueue

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class MeasurementDataReceiver(private val socket: DatagramSocket, private val dataFrameQueue: BlockingQueue<AbstractDataFrame>) {

    private val logger = EasyLogger.get(this.javaClass)
    private val rawPacketsQueue = LinkedBlockingQueue<DatagramPacket>()
    private var receiverThread: Thread? = null
    private var parserThread: Thread? = null
    private var isRunning: Boolean = false

    init {
        start()
    }

    private fun start() {
        isRunning = true

        // receiver
        receiverThread = Thread {
            while (isRunning) {
                try {
                    val buf = ByteArray(Katherine.MAX_DATA_PACKET_LENGTH)
                    val datagramPacket = DatagramPacket(buf, buf.size)
                    socket.receive(datagramPacket)
                    rawPacketsQueue.add(datagramPacket)
                } catch (e: SocketTimeoutException) {
                    logger.i("Receive timeout reach.")
                } catch (e: IOException) {
                    logger.e(e, "Exception while receiving data packet - %s.", e.message.toString())
                }

            }
        }
        receiverThread!!.start()

        // parser
        parserThread = Thread {
            while (isRunning) {
                try {
                    val packet = rawPacketsQueue.take()
                    processPacket(packet)
                } catch (e: InterruptedException) {
                    logger.e(e, "Packets queue take fail")
                }

            }
        }
        parserThread!!.start()
    }

    fun stop() {
        isRunning = false
    }

    private fun processPacket(packet: DatagramPacket) {
        val length = packet.length
        val data = packet.data

        if (length == 0 || length % DataFrame.LENGTH != 0) {
            logger.w("Packet payload is damaged and will be ignored because length = %d", length)
            return
        }

        var dataFrame: DataFrame
        var i = 0
        while (i < length) {
            try {
                dataFrame = DataFrame(System.currentTimeMillis(), Arrays.copyOfRange(data, i, i + DataFrame.LENGTH))
                logger.i("New data frame - %s", dataFrame.toString())
                dataFrameQueue.add(dataFrame)
            } catch (e: DetectorException) {
                logger.w("Unable to parse data frame #%d - %s", i, Arrays.toString(
                        Arrays.copyOfRange(data, i, i + DataFrame.LENGTH + 1)))
            }

            i += DataFrame.LENGTH
        }
    }

}
