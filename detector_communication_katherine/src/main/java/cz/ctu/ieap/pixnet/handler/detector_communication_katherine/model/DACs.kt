package cz.ctu.ieap.pixnet.handler.detector_communication_katherine.model

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
enum class DACs(val internalID: Int, val getterID: Int, val setterID: Int) {

    Ibias_Preamp_ON(0, 0, 1),
    Ibias_Preamp_OFF(1, 1, 2),
    VPreamp_NCAS(2, 2, 3),
    Ibias_Ikrum(3, 3, 4),
    Vfbk(4, 4, 5),
    Vthreshold_fine(5, 5, 6),
    Vthreshold_coarse(6, 6, 7),
    Ibias_DiscS1_ON(7, 7, 8),
    Ibias_DiscS1_OFF(8, 8, 9),
    Ibias_DiscS2_ON(9, 9, 10),
    Ibias_DiscS2_OFF(10, 10, 11),
    Ibias_PixelDAC(11, 11, 12),
    Ibias_TPbufferIn(12, 12, 13),
    Ibias_TPbufferOut(13, 13, 14),
    VTP_coarse(14, 14, 15),
    VTP_fine(15, 15, 16),
    Ibias_CP_PLL(16, 16, 17),
    PLL_Vcntrl(17, 17, 18),
    BandGap_output(18, -1, 28),
    BandGap_Temp(19, -1, 29),
    Ibias_dac(20, -1, 30),
    Ibias_dac_cas(21, -1, 31);

    fun hasSetter(): Boolean {
        return setterID != -1
    }

    fun hasGetter(): Boolean {
        return getterID != -1
    }

    companion object {

        fun getById(id: Int): DACs? {
            for (c in DACs.values()) {
                if (c.internalID == id) return c
            }
            return null
        }
    }
}