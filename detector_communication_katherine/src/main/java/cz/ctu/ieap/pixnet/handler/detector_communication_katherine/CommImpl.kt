package cz.ctu.ieap.pixnet.handler.detector_communication_katherine

import cz.ctu.ieap.pixnet.commons.exceptions.DetectorException
import cz.ctu.ieap.pixnet.commons.model.AbstractDataFrame
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.DetectorComm
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.commands.execution.AbstractExecutionCommand
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.commands.execution.ExecutionCommand
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.commands.execution.ExecutionCommandGroup
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.commands.execution.ValueId
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.commands.value.AbstractValueCommand
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.commands.value.ValueCommand
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.commands.value.ValueCommand.AccessType.*
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.commands.value.ValueCommandGroup
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.model.DetectorType
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.model.ValuePayload
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.model.ValueUnit
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.model.value_model.*
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.utils.ValueUtils.getBooleanValueById
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.utils.ValueUtils.getIntValueById
import cz.ctu.ieap.pixnet.handler.detector_communication_katherine.model.*
import cz.ctu.ieap.pixnet.handler.detector_communication_katherine.model.KatherineValueCommands.*
import cz.ctu.ieap.pixnet.handler.detector_communication_katherine.service.Katherine
import cz.ctu.ieap.pixnet.handler.detector_communication_katherine.service.Katherine.Companion.COMM_PACKET_LENGTH
import cz.ctu.ieap.pixnet.handler.detector_communication_katherine.service.Katherine.Companion.MAX_DATA_PACKET_LENGTH
import cz.ctu.ieap.pixnet.handler.detector_communication_katherine.utils.KathrineUtils
import cz.ctu.ieap.pixnet.kathrine_commons.model.ControlFrame
import cz.ctu.ieap.pixnet.kathrine_commons.model.DetectorConfig
import org.slf4j.LoggerFactory
import org.yaml.snakeyaml.Yaml
import org.yaml.snakeyaml.constructor.CustomClassLoaderConstructor
import java.io.IOException
import java.net.DatagramSocket
import java.net.SocketException
import java.util.*
import java.util.concurrent.BlockingQueue
import java.util.concurrent.LinkedBlockingQueue

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class CommImpl : DetectorComm {

    private val logger = LoggerFactory.getLogger(this.javaClass)

    private var katherine: Katherine? = null
    private val valueCommands = ArrayList<AbstractValueCommand>()
    private val executionCommands = ArrayList<AbstractExecutionCommand>()
    private val dataFrameQueue = LinkedBlockingQueue<AbstractDataFrame>()
    private var detectorConfig: DetectorConfig = DetectorConfig()
    private var callback: DetectorComm.Callback? = null

    init {
        initValueCommands()
        initExecutionCommands()

        // TODO test

        dataFrameQueue.add(ControlFrame(ControlFrame.Type.MEASUREMENT_STARTED))
    }

    private fun initValueCommands() {
        valueCommands.add(ValueCommand(
                ACQUISITION_TIME.id,
                "Acquisition time",
                ValueUnit.NANO_SECOND,
                SETTER,
                LongValueModel()
        ))
        valueCommands.add(ValueCommand(
                BIAS.id,
                "Bias",
                ValueUnit.VOLT,
                SETTER_AND_GETTER,
                FloatValueModel()
        ))
        valueCommands.add(ValueCommand(
                CHIP_ID.id,
                "ChipID",
                ValueUnit.STRING,
                SETTER_AND_GETTER,
                StringValueModel()
        ))
        // DACs
        valueCommands.add(ValueCommandGroup("DACs",
                ValueCommand(
                        DAC_Ibias_Preamp_ON.id,
                        "DAC_Ibias_Preamp_ON",
                        ValueUnit.DIMENSIONLESS,
                        SETTER_AND_GETTER,
                        IntValueModel(0, 255)
                ),
                ValueCommand(
                        DAC_Ibias_Preamp_OFF.id,
                        "DAC_Ibias_Preamp_OFF",
                        ValueUnit.DIMENSIONLESS,
                        SETTER_AND_GETTER,
                        IntValueModel(0, 255)
                ),
                ValueCommand(
                        DAC_VPreamp_NCAS.id,
                        "DAC_VPreamp_NCAS",
                        ValueUnit.DIMENSIONLESS,
                        SETTER_AND_GETTER,
                        IntValueModel(0, 255)
                ),
                ValueCommand(
                        DAC_Ibias_Ikrum.id,
                        "DAC_Ibias_Ikrum",
                        ValueUnit.DIMENSIONLESS,
                        SETTER_AND_GETTER,
                        IntValueModel(0, 255)
                ),
                ValueCommand(
                        DAC_Vfbk.id,
                        "DAC_Vfbk",
                        ValueUnit.DIMENSIONLESS,
                        SETTER_AND_GETTER,
                        IntValueModel(0, 255)
                ),
                ValueCommand(
                        DAC_Vthreshold_fine.id,
                        "DAC_Vthreshold_fine",
                        ValueUnit.DIMENSIONLESS,
                        SETTER_AND_GETTER,
                        IntValueModel(0, 511)
                ),
                ValueCommand(
                        DAC_Vthreshold_coarse.id,
                        "DAC_Vthreshold_coarse",
                        ValueUnit.DIMENSIONLESS,
                        SETTER_AND_GETTER,
                        IntValueModel(0, 255)
                ),
                ValueCommand(
                        DAC_Ibias_DiscS1_ON.id,
                        "DAC_Ibias_DiscS1_ON",
                        ValueUnit.DIMENSIONLESS,
                        SETTER_AND_GETTER,
                        IntValueModel(0, 255)
                ),
                ValueCommand(
                        DAC_Ibias_DiscS1_OFF.id,
                        "DAC_Ibias_DiscS1_OFF",
                        ValueUnit.DIMENSIONLESS,
                        SETTER_AND_GETTER,
                        IntValueModel(0, 255)
                ),
                ValueCommand(
                        DAC_Ibias_DiscS2_ON.id,
                        "DAC_Ibias_DiscS2_ON",
                        ValueUnit.DIMENSIONLESS,
                        SETTER_AND_GETTER,
                        IntValueModel(0, 255)
                ),
                ValueCommand(
                        DAC_Ibias_DiscS2_OFF.id,
                        "DAC_Ibias_DiscS2_OFF",
                        ValueUnit.DIMENSIONLESS,
                        SETTER_AND_GETTER,
                        IntValueModel(0, 255)
                ),
                ValueCommand(
                        DAC_Ibias_PixelDAC.id,
                        "DAC_Ibias_PixelDAC",
                        ValueUnit.DIMENSIONLESS,
                        SETTER_AND_GETTER,
                        IntValueModel(0, 255)
                ),
                ValueCommand(
                        DAC_Ibias_TPbufferIn.id,
                        "DAC_Ibias_TPbufferIn",
                        ValueUnit.DIMENSIONLESS,
                        SETTER_AND_GETTER,
                        IntValueModel(0, 255)
                ),
                ValueCommand(
                        DAC_Ibias_TPbufferOut.id,
                        "DAC_Ibias_TPbufferOut",
                        ValueUnit.DIMENSIONLESS,
                        SETTER_AND_GETTER,
                        IntValueModel(0, 255)
                ),
                ValueCommand(
                        DAC_VTP_coarse.id,
                        "DAC_VTP_coarse",
                        ValueUnit.DIMENSIONLESS,
                        SETTER_AND_GETTER,
                        IntValueModel(0, 255)
                ),
                ValueCommand(
                        DAC_VTP_fine.id,
                        "DAC_VTP_fine",
                        ValueUnit.DIMENSIONLESS,
                        SETTER_AND_GETTER,
                        IntValueModel(0, 255)
                ),
                ValueCommand(
                        DAC_Ibias_CP_PLL.id,
                        "DAC_Ibias_CP_PLL",
                        ValueUnit.DIMENSIONLESS,
                        SETTER_AND_GETTER,
                        IntValueModel(0, 255)
                ),
                ValueCommand(
                        DAC_PLL_Vcntrl.id,
                        "DAC_PLL_Vcntrl",
                        ValueUnit.DIMENSIONLESS,
                        SETTER_AND_GETTER,
                        IntValueModel(0, 255)
                ),
                ValueCommand(
                        DAC_BandGap_output.id,
                        "DAC_BandGap_output",
                        ValueUnit.DIMENSIONLESS,
                        GETTER,
                        IntValueModel(0, 255)
                ),
                ValueCommand(
                        DAC_BandGap_Temp.id,
                        "DAC_BandGap_Temp",
                        ValueUnit.DIMENSIONLESS,
                        GETTER,
                        IntValueModel(0, 255)
                ),
                ValueCommand(
                        DAC_Ibias_dac.id,
                        "DAC_Ibias_dac",
                        ValueUnit.DIMENSIONLESS,
                        GETTER,
                        IntValueModel(0, 255)
                ),
                ValueCommand(
                        DAC_Ibias_dac_cas.id,
                        "DAC_Ibias_dac_cas",
                        ValueUnit.DIMENSIONLESS,
                        GETTER,
                        IntValueModel(0, 255)
                )
        ))
        // Sensor register settings
        //TODO - add ranges
        valueCommands.add(ValueCommandGroup(
                "Sensor registers",
                ValueCommand(
                        SR_TEST_PULSE_PERIOD.id,
                        "TEST_PULSE_PERIOD",
                        ValueUnit.DIMENSIONLESS,
                        SETTER,
                        IntValueModel(0, 255)
                ),
                ValueCommand(
                        SR_NUMBER_TEST_PULSES.id,
                        "NUMBER_TEST_PULSES",
                        ValueUnit.DIMENSIONLESS,
                        SETTER,
                        IntValueModel(0, 255)
                ),
                ValueCommand(
                        SR_OUT_BLOCK_CONFIG.id,
                        "OUT_BLOCK_CONFIG",
                        ValueUnit.DIMENSIONLESS,
                        SETTER,
                        IntValueModel(0, 255)
                ),
                ValueCommand(
                        SR_PLL_CONFIG.id,
                        "PLL_CONFIG",
                        ValueUnit.DIMENSIONLESS,
                        SETTER,
                        IntValueModel(0, 255)
                ),
                ValueCommand(
                        SR_GENERAL_CONFIG.id,
                        "GENERAL_CONFIG",
                        ValueUnit.DIMENSIONLESS,
                        SETTER,
                        IntValueModel(0, 255)
                ),
                ValueCommand(
                        SR_SLVS_CONFIG.id,
                        "SLVS_CONFIG",
                        ValueUnit.DIMENSIONLESS,
                        SETTER,
                        IntValueModel(0, 255)
                ),
                ValueCommand(
                        SR_POWER_PULSING_PATTERN.id,
                        "POWER_PULSING_PATTERN",
                        ValueUnit.DIMENSIONLESS,
                        SETTER,
                        IntValueModel(0, 255)
                ),
                ValueCommand(
                        SR_SENSE_DAC_SELECTOR.id,
                        "SENSE_DAC_SELECTOR",
                        ValueUnit.DIMENSIONLESS,
                        SETTER,
                        IntValueModel(0, 255)
                ),
                ValueCommand(
                        SR_EXT_DAC_SELECTOR.id,
                        "EXT_DAC_SELECTOR",
                        ValueUnit.DIMENSIONLESS,
                        SETTER,
                        IntValueModel(0, 255)
                )
                // TODO
                //                new ValueCommand(
                //                        SR_SET_TIMER_VIRTUAL.id,
                //                        "SET_TIMER_VIRTUAL",
                //                        ValueUnit.DIMENSIONLESS,
                //                        new IntValueModel(GETTER, 0, 255)
                //                )
        ))
    }

    private fun initExecutionCommands() {
        executionCommands.add(ExecutionCommand(
                KatherineExecutionCommands.SET_ACQ_MODE.internalID,
                "Set acquisition mode",
                arrayOf(ValueModelVerbose(
                        "Acquisition mode",
                        ValueId.acq_mode.name,
                        IntValueModel(mapOf(
                                Pair("ToA & ToT", 0),
                                Pair("ToA", 1),
                                Pair("Event & iToT", 2)
                        )),
                        ValueUnit.DIMENSIONLESS
                ), ValueModelVerbose(
                        "Fast vco enabled",
                        ValueId.fast_vco_en.name,
                        BooleanValueModel(mapOf(
                                Pair("Enable", true),
                                Pair("Disable", false)
                        )),
                        ValueUnit.DIMENSIONLESS
                )), null
        ))
        executionCommands.add(ExecutionCommand(
                KatherineExecutionCommands.SET_NUMBER_OF_FRAMES.internalID,
                "Set number of frames",
                arrayOf(ValueModelVerbose(
                        "Number of frames",
                        ValueId.frames_count.name,
                        IntValueModel(0, Integer.MAX_VALUE),
                        ValueUnit.DIMENSIONLESS
                )), null

        ))
        executionCommands.add(ExecutionCommand(
                KatherineExecutionCommands.MEASURE_READOUT_TEMPERATURE.internalID,
                "Measure temperature of readout", null,
                arrayOf(ValueModelVerbose(
                        "Temperature",
                        ValueId.temperature.name,
                        FloatValueModel(0f, java.lang.Float.MAX_VALUE),
                        ValueUnit.CELSIUS
                ))

        ))
        executionCommands.add(ExecutionCommand(
                KatherineExecutionCommands.MEASURE_SENSOR_TEMPERATURE.internalID,
                "Measure temperature of sensor", null,
                arrayOf(ValueModelVerbose(
                        "Temperature",
                        ValueId.temperature.name,
                        FloatValueModel(0f, java.lang.Float.MAX_VALUE),
                        ValueUnit.CELSIUS
                ))

        ))
        executionCommands.add(ExecutionCommand(
                KatherineExecutionCommands.GET_READOUT_STATUS.internalID,
                "Get status information of readout HW", null,
                arrayOf(ValueModelVerbose(
                        "HW type",
                        ValueId.hw_type.name,
                        IntValueModel(0, 255),
                        ValueUnit.DIMENSIONLESS
                ), ValueModelVerbose(
                        "HW revision",
                        ValueId.hw_revision.name,
                        IntValueModel(0, 255),
                        ValueUnit.DIMENSIONLESS
                ), ValueModelVerbose(
                        "HW serial number",
                        ValueId.hw_serial_number.name,
                        IntValueModel(0, 255 * 255),
                        ValueUnit.DIMENSIONLESS
                ), ValueModelVerbose(
                        "FW version",
                        ValueId.fw_version.name,
                        IntValueModel(0, 255 * 255),
                        ValueUnit.DIMENSIONLESS
                ))

        ))
        executionCommands.add(ExecutionCommand(
                KatherineExecutionCommands.START_HW_COMMAND.internalID,
                "Start HW Command",
                arrayOf(ValueModelVerbose(
                        "HW command ID",
                        ValueId.readout_hw_command_id.name,
                        IntValueModel(mapOf(
                                Pair("Sensor Config Registers Update", 0),
                                Pair("Internal DAC Update", 1),
                                Pair("Internal DAC Back Read", 2),
                                Pair("Timer Read", 3),
                                Pair("Timer Set", 4),
                                Pair("Reset Matrix Sequential", 5),
                                Pair("Stop Matrix Command", 6),
                                Pair("Load Column Test Pulse Register", 7),
                                Pair("Read Column Test Pulse Register", 8),
                                Pair("Load Pixel Register Configuration", 9),
                                Pair("Read Pixel Register Configuration", 10),
                                Pair("Read Pixel Matrix Sequential Setting", 11),
                                Pair("Read Pixel Matrix Data-Driven Setting", 12),
                                Pair("Chip ID Read", 13),
                                Pair("Output Block Config Update", 14),
                                Pair("Digital Test", 15)
                        )),
                        ValueUnit.DIMENSIONLESS
                )), null

        ))
        executionCommands.add(ExecutionCommand(
                KatherineExecutionCommands.PERFORM_DIGITAL_TEST.internalID,
                "Perform digital test", null,
                arrayOf(ValueModelVerbose(
                        "Test result",
                        ValueId.digital_test_result.name,
                        StringValueModel(),
                        ValueUnit.DIMENSIONLESS
                ))

        ))
        executionCommands.add(ExecutionCommand(
                KatherineExecutionCommands.ACQ_SETUP.internalID,
                "Start acquisition", null,
                arrayOf(ValueModelVerbose(
                        "Start mode",
                        ValueId.start_mode.name,
                        IntValueModel(mapOf(
                                Pair("Start immediately", 0),
                                Pair("Start by trigger", 1)
                        )),
                        ValueUnit.DIMENSIONLESS
                ), ValueModelVerbose(
                        "HW start trigger channel",
                        ValueId.hw_start_trigger_channel.name,
                        IntValueModel(0, 7),
                        ValueUnit.DIMENSIONLESS
                ), ValueModelVerbose(
                        "Edge setup start",
                        ValueId.edge_setup_start.name,
                        IntValueModel(mapOf(
                                Pair("Rising edge of trigger", 0),
                                Pair("Falling edge of trigger", 1)
                        )),
                        ValueUnit.DIMENSIONLESS
                ), ValueModelVerbose(
                        "Delayed start by HW event",
                        ValueId.delayed_start_by_hw_event.name,
                        IntValueModel(mapOf(
                                Pair("Start of acq. immediately", 0),
                                Pair("Start of acq. is delayed", 1)
                        )),
                        ValueUnit.DIMENSIONLESS
                ), ValueModelVerbose(
                        "Acquisition finish",
                        ValueId.acq_finish.name,
                        IntValueModel(mapOf(
                                Pair("By timer", 0),
                                Pair("Hy trigger", 1)
                        )),
                        ValueUnit.DIMENSIONLESS
                ), ValueModelVerbose(
                        "HW finish trigger channel",
                        ValueId.hw_finish_trigger_channel.name,
                        IntValueModel(0, 7),
                        ValueUnit.DIMENSIONLESS
                ), ValueModelVerbose(
                        "Edge setup finish",
                        ValueId.edge_setup_finish.name,
                        IntValueModel(mapOf(
                                Pair("Rising edge of trigger", 0),
                                Pair("Falling edge of trigger", 1)
                        )),
                        ValueUnit.DIMENSIONLESS
                ))

        ))
        executionCommands.add(ExecutionCommand(
                KatherineExecutionCommands.ACQ_START.internalID,
                "Start acquisition", null,
                arrayOf(ValueModelVerbose(
                        "Readout mode",
                        ValueId.readout_mode.name,
                        IntValueModel(mapOf(
                                Pair("sequential", 0),
                                Pair("data-driven", 1)
                        )),
                        ValueUnit.DIMENSIONLESS
                ))
        ))
        executionCommands.add(ExecutionCommand(
                KatherineExecutionCommands.ACQ_STOP.internalID,
                "Stop acquisition", null, null
        ))
    }

    override fun getDetectorType(): DetectorType {
        return DetectorType.TIMEPIX3
    }

    override fun getReadoutName(): String {
        return "katherine"
    }

    override fun getSensorsCount(): Int {
        return 1
    }

    override fun getDetectorWidth(): Int {
        return 256
    }

    override fun getDetectorHeight(): Int {
        return 256
    }

    override fun getDetectorConfig(): String? {
        val yaml = if (callback != null && callback!!.classLoader != null) {
            Yaml(CustomClassLoaderConstructor(callback!!.classLoader))
        } else {
            Yaml()
        }
        return yaml.dump(detectorConfig)
    }

    override fun setDetectorConfig(config: String) {
        detectorConfig = DetectorConfig.fromYaml(config, callback?.classLoader)
    }

    override fun getSupportedValueCommands(): List<AbstractValueCommand> {
        return valueCommands
    }

    override fun getSupportedExecutionCommands(): List<AbstractExecutionCommand> {
        return executionCommands
    }

    @Throws(DetectorException::class)
    override fun connect(): Boolean {

        // create command socket
        val socketCmd: DatagramSocket
        try {
            socketCmd = detectorConfig.portCommandsToListen?.let {
                DatagramSocket(it)
            } ?: DatagramSocket()

            socketCmd.receiveBufferSize = COMM_PACKET_LENGTH
            socketCmd.soTimeout = 60000
        } catch (e: SocketException) {
            logger.error("Could not connect to command socket command socket", e)
            throw DetectorException(e, "Could not connect to command socket.")
        }

        // create data socket
        val socketData: DatagramSocket
        try {
            socketData = detectorConfig.portDataToListen?.let {
                DatagramSocket(it)
            } ?: DatagramSocket()

            socketData.receiveBufferSize = MAX_DATA_PACKET_LENGTH
            socketData.soTimeout = 60000

            // create Katherine instance
            katherine = Katherine(socketCmd, socketData, detectorConfig, dataFrameQueue)
            return true
        } catch (e: IOException) {
            logger.error("Could not connect to data socket", e)
            throw DetectorException(e, "Could not connect to data socket.")
        }

    }

    @Throws(DetectorException::class)
    override fun disconnect(): Boolean {
        if (katherine == null) {
            logger.warn("Detector is already disconnected")
            throw DetectorException("Detector is already disconnected")
        }
        katherine!!.disconnect()
        katherine = null
        return true
    }

    override fun isConnected(): Boolean {
        return katherine?.isConnected ?: false
    }

    @Throws(DetectorException::class)
    override fun executeSetValueCommand(commandID: Int, payload: ValuePayload) {
        checkConnection()
        val command = findValueCommandById(commandID)
        val katherineCommand = KatherineValueCommands.getById(commandID)
        if (command == null || katherineCommand == null) {
            throw DetectorException("Unknown command with id=%d.", commandID)
        }
        if (command.accessType == null || command.accessType?.setAllowed() == false) {
            throw DetectorException("Wrong access type.")
        }

        // Sensor registers commands
        if (katherineCommand.id >= 2000) {
            val sr = SensorRegisters.getById(katherineCommand.id - 2000)
                    ?: throw DetectorException("Sensor register with id %d not found.", katherineCommand.id)
            katherine!!.setSensorRegister(sr.id, payload.intValue!!)
            return
        }

        // DACs commands
        if (katherineCommand.id >= 1000) {
            val dac = DACs.getById(katherineCommand.id - 1000)
                    ?: throw DetectorException("DAC with id %d not found.", katherineCommand.id)
            katherine!!.setDac(dac.getterID, payload.intValue!!)
            return
        }

        // others commands
        when (katherineCommand) {
            ACQUISITION_TIME -> katherine!!.setAcquisitionTime(payload.longValue!!)
            BIAS -> katherine!!.setBias(payload.floatValue!!)
            else -> throw DetectorException("Command %s not implemented", katherineCommand.name)
        }
    }

    @Throws(DetectorException::class)
    override fun executeGetValueCommand(commandID: Int): ValuePayload {
        checkConnection()
        val command = findValueCommandById(commandID)
        val katherineCommand = KatherineValueCommands.getById(commandID)
        if (command == null || katherineCommand == null) {
            throw DetectorException("Unknown command with id=%d.", commandID)
        }
        if (command.accessType == null || command.accessType?.allowed == false) {
            throw DetectorException("Wrong access type.")
        }

        // DACs commands
        if (katherineCommand.id >= 1000) {
            val dac = DACs.getById(katherineCommand.id)
                    ?: throw DetectorException("DAC with id %d not found.", katherineCommand.id)
            return ValuePayload(katherine!!.getDac(dac.getterID))
        }

        // others commands
        return when (katherineCommand) {
            BIAS -> ValuePayload(katherine!!.bias)
            CHIP_ID -> ValuePayload(katherine!!.chipId)
            else -> throw DetectorException("Command %s not implemented", katherineCommand.name)
        }
    }

    @Throws(DetectorException::class)
    override fun executeExecutionCommand(commandID: Int, input: Map<String, ValuePayload>): Map<String, ValuePayload> {
        checkConnection()
        val command = findExecutionCommandById(commandID)
        val katherineCommand = KatherineExecutionCommands.getById(commandID)
        if (command == null || katherineCommand == null) {
            throw DetectorException("Unknown command with id=%d.", commandID)
        }

        when (katherineCommand) {
            KatherineExecutionCommands.SET_ACQ_MODE -> return katherine!!.setAcqMode(
                    getIntValueById(input, ValueId.acq_mode.name),
                    getBooleanValueById(input, ValueId.fast_vco_en.name)
            )
            KatherineExecutionCommands.SET_NUMBER_OF_FRAMES -> return katherine!!.setNumberOfFrames(
                    getIntValueById(input, ValueId.frames_count.name)
            )
            KatherineExecutionCommands.MEASURE_READOUT_TEMPERATURE -> return katherine!!.measureReadoutTemperature()
            KatherineExecutionCommands.MEASURE_SENSOR_TEMPERATURE -> return katherine!!.measureSensorTemperature()
            KatherineExecutionCommands.GET_READOUT_STATUS -> return katherine!!.readoutStatus
            KatherineExecutionCommands.START_HW_COMMAND -> return katherine!!.startHwCommand(
                    getIntValueById(input, ValueId.readout_hw_command_id.name)
            )
            KatherineExecutionCommands.PERFORM_DIGITAL_TEST -> return katherine!!.performDigitalTest()
            KatherineExecutionCommands.ACQ_SETUP -> return katherine!!.acqSetup(
                    getIntValueById(input, ValueId.start_mode.name),
                    getIntValueById(input, ValueId.hw_start_trigger_channel.name),
                    getIntValueById(input, ValueId.edge_setup_start.name),
                    getIntValueById(input, ValueId.delayed_start_by_hw_event.name),
                    getIntValueById(input, ValueId.acq_finish.name),
                    getIntValueById(input, ValueId.hw_finish_trigger_channel.name),
                    getIntValueById(input, ValueId.edge_setup_finish.name)
            )
            KatherineExecutionCommands.ACQ_START -> return katherine!!.acqStart(
                    getIntValueById(input, ValueId.readout_mode.name)
            )
            KatherineExecutionCommands.ACQ_STOP -> return katherine!!.acqStop()
            else -> throw DetectorException("Command %s not implemented", katherineCommand.name)
        }
    }

    override fun getAcceptedFilesKeys(): List<String> {
        return listOf(AcceptedFiles.pixel_config.name)
    }

    @Throws(DetectorException::class)
    override fun uploadFile(fileKey: String, file: ByteArray?) {
        checkConnection()
        val fileType = AcceptedFiles.getByName(fileKey) ?: throw DetectorException("Unknown fileKey - %s.", fileKey)

        when (fileType) {
            AcceptedFiles.pixel_config -> {
                if (file == null || file.size != 65536) {
                    throw DetectorException("Wrong config length. Expected 65536, but %s bytes given.",
                            file?.size?.toString() ?: "no")
                }

                val config = KathrineUtils.bmcToMatrixConfig(file)
                katherine!!.uploadDetectorConfig(config)
                throw DetectorException("Upload of file with key %s not implemented", fileType.name)
            }
            else -> throw DetectorException("Upload of file with key %s not implemented", fileType.name)
        }
    }

    override fun getDataFrameQueue(): BlockingQueue<AbstractDataFrame> {
        return dataFrameQueue
    }

    private fun findValueCommandById(id: Int): ValueCommand? {
        for (item in valueCommands) {
            if (item is ValueCommand) {
                if (id == item.id) return item
            } else if (item is ValueCommandGroup) {
                for (valueCommand in item.valueCommands) {
                    if (id == valueCommand.id) return valueCommand
                }
            }
        }
        return null
    }

    override fun setCallback(callback: DetectorComm.Callback) {
        this.callback = callback
    }

    private fun findExecutionCommandById(id: Int): ExecutionCommand? {
        for (item in executionCommands) {
            if (item is ExecutionCommand) {
                if (id == item.id) return item
            } else if (item is ExecutionCommandGroup) {
                for (executionCommand in item.executionCommands) {
                    if (id == executionCommand.id) return executionCommand
                }
            }
        }
        return null
    }

    @Throws(DetectorException::class)
    private fun checkConnection() {
        if (!isConnected) throw DetectorException("Detector is not connected.")
    }

}