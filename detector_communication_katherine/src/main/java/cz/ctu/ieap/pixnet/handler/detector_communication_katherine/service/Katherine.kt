package cz.ctu.ieap.pixnet.handler.detector_communication_katherine.service

import cz.ctu.ieap.pixnet.commons.exceptions.DetectorException
import cz.ctu.ieap.pixnet.commons.model.AbstractDataFrame
import cz.ctu.ieap.pixnet.commons.utils.ByteUtils
import cz.ctu.ieap.pixnet.commons.utils.EasyLogger
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.commands.execution.ValueId
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.model.ValuePayload
import cz.ctu.ieap.pixnet.handler.detector_communication_katherine.model.KatherineExecutionCommands
import cz.ctu.ieap.pixnet.handler.detector_communication_katherine.model.KatherineValueCommands
import cz.ctu.ieap.pixnet.handler.detector_communication_katherine.utils.PacketBuilder
import cz.ctu.ieap.pixnet.kathrine_commons.model.AcqConfigurationFrame
import cz.ctu.ieap.pixnet.kathrine_commons.model.ControlFrame
import cz.ctu.ieap.pixnet.kathrine_commons.model.DetectorConfig
import java.io.IOException
import java.net.DatagramSocket
import java.net.InetAddress
import java.util.concurrent.BlockingQueue

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class Katherine @Throws(IOException::class)
constructor(private var commSocket: DatagramSocket?, private var dataSocket: DatagramSocket?, private val detectorConfig: DetectorConfig,
            private val dataFrameQueue: BlockingQueue<AbstractDataFrame>) {

    private val logger = EasyLogger.get(this.javaClass)
    private val packetSender: PacketSender
    private val packetReceiver: PacketReceiver
    private val measurementDataReceiver: MeasurementDataReceiver

    companion object {
        const val COMM_PACKET_LENGTH = 8
        const val MAX_DATA_PACKET_LENGTH = 7000
        const val RESPONSE_TIMEOUT: Long = 20000
    }

    val isConnected: Boolean
        get() = commSocket != null && dataSocket != null

    init {

        val commSocket = this.commSocket ?: throw DetectorException("Comm socket not given")
        val dataSocket = this.dataSocket ?: throw DetectorException("Data socket not given")

        packetSender = PacketSender(commSocket, InetAddress.getByName(detectorConfig.host), detectorConfig.portCommands)
        packetReceiver = PacketReceiver(commSocket, COMM_PACKET_LENGTH, RESPONSE_TIMEOUT)

        measurementDataReceiver = MeasurementDataReceiver(dataSocket, dataFrameQueue)
    }

    fun disconnect() {
        commSocket?.disconnect()
        dataSocket?.disconnect()
    }

    //******************************************************************************************************************
    // VALUE COMMANDS
    //******************************************************************************************************************

    val chipId: String
        @Throws(DetectorException::class)
        get() {
            packetSender.send(
                    PacketBuilder.builder()
                            .commandID(KatherineValueCommands.CHIP_ID.getterCommandId)
                            .build()
            )
            val packet = packetReceiver
                    .pollPacketFromQueue(KatherineValueCommands.CHIP_ID.getterCommandId, RESPONSE_TIMEOUT)
            val intValue = packet.intValue
            val x = (intValue and 0xF) - 1
            val y = intValue shr 4 and 0xF
            val w = intValue shr 8 and 0xFFF
            return String.format("%c%d-W000%d", 65 + x, y, w)
        }

    val bias: Float
        @Throws(DetectorException::class)
        get() {
            packetSender.send(
                    PacketBuilder.builder()
                            .commandID(KatherineValueCommands.BIAS.getterCommandId)
                            .build()
            )
            val packet = packetReceiver
                    .pollPacketFromQueue(KatherineValueCommands.BIAS.getterCommandId, RESPONSE_TIMEOUT)
            return packet.floatValue
        }

    val readoutStatus: Map<String, ValuePayload>
        @Throws(DetectorException::class)
        get() {
            val commandId = KatherineExecutionCommands.GET_READOUT_STATUS.commandID
            packetSender.send(
                    PacketBuilder.builder()
                            .commandID(commandId)
                            .build()
            )
            val packet = packetReceiver
                    .pollPacketFromQueue(commandId, RESPONSE_TIMEOUT)
            return mapOf(
                    Pair(ValueId.hw_type.name, ValuePayload(packet.getIntValue(0, 1))),
                    Pair(ValueId.hw_revision.name, ValuePayload(packet.getIntValue(1, 2))),
                    Pair(ValueId.hw_serial_number.name, ValuePayload(packet.getIntValue(2, 4))),
                    Pair(ValueId.fw_version.name, ValuePayload(packet.getIntValue(4, 6)))
            )

        }

    @Throws(DetectorException::class)
    fun setBias(bias: Float): Boolean {
        logger.i(String.format("Setting bias %f V", bias))
        packetSender.send(
                PacketBuilder.builder()
                        .commandID(KatherineValueCommands.BIAS.setterCommandId)
                        .putFloat(bias)
                        .build()
        )
        packetReceiver
                .pollPacketFromQueue(KatherineValueCommands.BIAS.setterCommandId, RESPONSE_TIMEOUT)
                .checkAck()
        detectorConfig.bias = bias
        return true
    }


    @Throws(DetectorException::class)
    fun setAcquisitionTime(acqTime: Long) {

        // MSB
        packetSender.send(
                PacketBuilder.builder()
                        .commandID(KatherineValueCommands.ACQUISITION_TIME.setterCommandIdMSB)
                        .putLongMSB(acqTime)
                        .build()
        )
        packetReceiver
                .pollPacketFromQueue(KatherineValueCommands.ACQUISITION_TIME.setterCommandIdMSB, RESPONSE_TIMEOUT)
                .checkAck()

        // LSB
        packetSender.send(
                PacketBuilder.builder()
                        .commandID(KatherineValueCommands.ACQUISITION_TIME.setterCommandIdLSB)
                        .putLongLSB(acqTime)
                        .build()
        )
        packetReceiver
                .pollPacketFromQueue(KatherineValueCommands.ACQUISITION_TIME.setterCommandIdLSB, RESPONSE_TIMEOUT)
                .checkAck()
        detectorConfig.acqTime = acqTime

    }

    @Throws(DetectorException::class)
    fun getDac(dacId: Int): Float {
        val commandId = KatherineValueCommands.DAC_VIRTUAL.getterCommandId
        packetSender.send(
                PacketBuilder.builder()
                        .commandID(commandId)
                        .putByte(5, dacId.toByte())
                        .build()
        )
        val packet = packetReceiver
                .pollPacketFromQueue(commandId, RESPONSE_TIMEOUT)
        return packet.floatValue
    }

    @Throws(DetectorException::class)
    fun setDac(dacId: Int, value: Int) {
        val commandId = KatherineValueCommands.DAC_VIRTUAL.setterCommandId
        packetSender.send(
                PacketBuilder.builder()
                        .commandID(commandId)
                        .putInt(value)
                        .putByte(5, dacId.toByte())
                        .build()
        )
        packetReceiver
                .pollPacketFromQueue(commandId, RESPONSE_TIMEOUT)
                .checkAck()
        startHwCommand(1) // DAC update
    }

    @Throws(DetectorException::class)
    fun setSensorRegister(id: Int, value: Int) {
        val commandId = KatherineValueCommands.SR_VIRTUAL.setterCommandId
        packetSender.send(
                PacketBuilder.builder()
                        .commandID(commandId)
                        .putInt(value)
                        .putByte(5, id.toByte())
                        .build()
        )
        packetReceiver
                .pollPacketFromQueue(commandId, RESPONSE_TIMEOUT)
                .checkAck()
    }

    //******************************************************************************************************************
    // EXECUTION COMMANDS
    //******************************************************************************************************************

    @Throws(DetectorException::class)
    fun setAcqMode(acq_mode: Int, fast_vco_en: Boolean): Map<String, ValuePayload> {
        val commandId = KatherineExecutionCommands.SET_ACQ_MODE.commandID
        var value = 0
        if (fast_vco_en) {
            value += 256
        }
        value += acq_mode
        packetSender.send(
                PacketBuilder.builder()
                        .commandID(commandId)
                        .putInt(value)
                        .build()
        )
        packetReceiver
                .pollPacketFromQueue(commandId, RESPONSE_TIMEOUT)
                .checkAck()
        detectorConfig.acqMode = DetectorConfig.AcqMode.find(acq_mode, fast_vco_en)
        return mapOf()
    }

    @Throws(DetectorException::class)
    fun setNumberOfFrames(frames_count: Int): Map<String, ValuePayload> {
        val commandId = KatherineExecutionCommands.SET_NUMBER_OF_FRAMES.commandID
        packetSender.send(
                PacketBuilder.builder()
                        .commandID(commandId)
                        .putInt(frames_count)
                        .build()
        )
        packetReceiver
                .pollPacketFromQueue(commandId, RESPONSE_TIMEOUT)
                .checkAck()
        detectorConfig.numberOfFrames = frames_count
        return mapOf()
    }

    @Throws(DetectorException::class)
    fun measureReadoutTemperature(): Map<String, ValuePayload> {
        val commandId = KatherineExecutionCommands.MEASURE_READOUT_TEMPERATURE.commandID
        packetSender.send(
                PacketBuilder.builder()
                        .commandID(commandId)
                        .build()
        )
        val packet = packetReceiver
                .pollPacketFromQueue(commandId, RESPONSE_TIMEOUT)
        return mapOf(
                Pair(ValueId.temperature.name, ValuePayload(packet.floatValue))
        )
    }

    @Throws(DetectorException::class)
    fun measureSensorTemperature(): Map<String, ValuePayload> {
        val commandId = KatherineExecutionCommands.MEASURE_SENSOR_TEMPERATURE.commandID
        packetSender.send(
                PacketBuilder.builder()
                        .commandID(commandId)
                        .build()
        )
        val packet = packetReceiver
                .pollPacketFromQueue(commandId, RESPONSE_TIMEOUT)
        return mapOf(
                Pair(ValueId.temperature.name, ValuePayload(packet.floatValue))
        )
    }

    @Throws(DetectorException::class)
    fun startHwCommand(hwCommandId: Int): Map<String, ValuePayload> {
        val commandId = KatherineExecutionCommands.START_HW_COMMAND.commandID
        packetSender.send(
                PacketBuilder.builder()
                        .commandID(commandId)
                        .putInt(hwCommandId)
                        .build()
        )
        packetReceiver
                .pollPacketFromQueue(commandId, RESPONSE_TIMEOUT)
                .checkAck()
        return mapOf()
    }

    @Throws(DetectorException::class)
    fun performDigitalTest(): Map<String, ValuePayload> {
        val commandId = KatherineExecutionCommands.PERFORM_DIGITAL_TEST.commandID
        packetSender.send(
                PacketBuilder.builder()
                        .commandID(commandId)
                        .build()
        )
        val packet = packetReceiver
                .pollPacketFromQueue(commandId, RESPONSE_TIMEOUT)
        val response = packet.getIntValue(0, 1)
        return mapOf(
                Pair(ValueId.digital_test_result.name, ValuePayload(
                        if (response == 64) "SUCCESS (64)" else String.format("FAIL (%d)", response)
                ))
        )
    }

    fun acqSetup(start_mode_mode: Int, hw_start_trigger_channel: Int, edge_setup_start: Int,
                 delayed_start_by_hw_event: Int, acq_finish_mode: Int,
                 hw_finish_trigger_channel: Int, edge_setup_finish: Int): Map<String, ValuePayload> {
        val data = ("000"                                                        // 15..13

                + ByteUtils.getLastNBites(edge_setup_finish.toByte(), 1)          // 12

                + ByteUtils.getLastNBites(hw_finish_trigger_channel.toByte(), 3)  // 11..9

                + ByteUtils.getLastNBites(acq_finish_mode.toByte(), 1)            // 8

                + "00"                                                             // 7..6

                + ByteUtils.getLastNBites(delayed_start_by_hw_event.toByte(), 1)  // 5

                + ByteUtils.getLastNBites(edge_setup_start.toByte(), 1)           // 4

                + ByteUtils.getLastNBites(hw_start_trigger_channel.toByte(), 3)   // 3..1

                + ByteUtils.getLastNBites(start_mode_mode.toByte(), 1))           // 0
        val commandId = KatherineExecutionCommands.ACQ_SETUP.commandID
        packetSender.send(
                PacketBuilder.builder()
                        .commandID(commandId)
                        .putByte(0, java.lang.Byte.parseByte(data.substring(0, 8)))
                        .putByte(1, java.lang.Byte.parseByte(data.substring(8, 16)))
                        .putByte(4, 0x05.toByte())
                        .build()
        )

        // TODO ACQ response isn't implemented by Kathrine so far
        // packetReceiver.pollPacketFromQueue(commandId, RESPONSE_TIMEOUT).checkAck();
        detectorConfig.triggerConfig = DetectorConfig.TriggerConfig(
                DetectorConfig.TriggerConfig.StartMode.byValue(start_mode_mode),
                DetectorConfig.TriggerConfig.FinishMode.byValue(acq_finish_mode),
                hw_start_trigger_channel,
                hw_finish_trigger_channel,
                DetectorConfig.TriggerConfig.EdgeSetup.byValue(edge_setup_start),
                DetectorConfig.TriggerConfig.EdgeSetup.byValue(edge_setup_finish),
                delayed_start_by_hw_event == 1
        )
        return mapOf()
    }

    @Throws(DetectorException::class)
    fun acqStart(readoutMode: Int): Map<String, ValuePayload> {

        val readOutModeEnum = DetectorConfig.ReadOutMode.byValue(readoutMode)
                ?: throw DetectorException("Readout mode not set")
        val acqConfigurationFrame = AcqConfigurationFrame(
                detectorConfig.acqModeOrThrow,
                System.currentTimeMillis(),
                chipId,
                readOutModeEnum,
                detectorConfig.biasOrThrow,
                measureSensorTemperature()[ValueId.temperature.name]?.floatValue ?: 0f,
                measureReadoutTemperature()[ValueId.temperature.name]?.floatValue ?: 0f
        )
        dataFrameQueue.add(ControlFrame(ControlFrame.Type.MEASUREMENT_STARTED))
        dataFrameQueue.add(acqConfigurationFrame)
        val commandId = KatherineExecutionCommands.ACQ_START.commandID
        packetSender.send(
                PacketBuilder.builder()
                        .commandID(commandId)
                        .putInt(readoutMode)
                        .build()
        )
        // TODO ACQ response isn't implemented by Kathrine so far
        // packetReceiver.pollPacketFromQueue(commandId, RESPONSE_TIMEOUT).checkAck();
        detectorConfig.readoutMode = readOutModeEnum
        return mapOf()
    }

    @Throws(DetectorException::class)
    fun acqStop(): Map<String, ValuePayload> {
        val commandId = KatherineExecutionCommands.ACQ_STOP.commandID
        packetSender.send(
                PacketBuilder.builder()
                        .commandID(commandId)
                        .build()
        )
        // TODO ACQ response isn't implemented by Kathrine so far
        // packetReceiver.pollPacketFromQueue(commandId, RESPONSE_TIMEOUT).checkAck();
        return mapOf()
    }

    @Throws(DetectorException::class)
    fun uploadDetectorConfig(config: ByteArray) {
        val commandId = KatherineExecutionCommands.UPLOAD_PIXEL_CONFIG.commandID
        packetSender.send(
                PacketBuilder.builder()
                        .commandID(commandId)
                        .build()
        )
        packetSender.send(config)
        packetReceiver
                .pollPacketFromQueue(commandId, RESPONSE_TIMEOUT)
                .checkAck()

        startHwCommand(5) // reset matrix
        startHwCommand(9) // save matrix
    }

}
