package cz.ctu.ieap.pixnet.handler.detector_communication_katherine.model

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
enum class AcceptedFiles {

    pixel_config;

    companion object {

        fun getByName(name: String): AcceptedFiles? {
            for (acceptedFile in AcceptedFiles.values()) {
                if (acceptedFile.name.startsWith(name)) return acceptedFile
            }
            return null
        }
    }
}
