package cz.ctu.ieap.pixnet.handler.detector_communication_katherine.service

import cz.ctu.ieap.pixnet.commons.utils.EasyLogger
import java.io.IOException
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress
import java.util.*
import java.util.concurrent.ArrayBlockingQueue

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class PacketSender(private val socket: DatagramSocket, private val address: InetAddress, private val port: Int) {

    private val logger = EasyLogger.get(this.javaClass)
    private val queue = ArrayBlockingQueue<DatagramPacket>(1024)
    private val senderThread: Thread
    private var isRunning: Boolean = false

    init {
        senderThread = Thread {
            isRunning = true
            while (isRunning) {
                try {
                    val packet = queue.take()
                    socket.send(packet)
                    logger.i("Sending packet: %s", Arrays.toString(packet.data))
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }
        }
        senderThread.start()
    }

    fun send(buff: ByteArray) {
        send(DatagramPacket(buff, buff.size, address, port))
    }

    private fun send(packet: DatagramPacket) {
        try {
            queue.put(packet)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

    }

    fun stop() {
        isRunning = false
        senderThread.interrupt()
    }
}
