package cz.ctu.ieap.pixnet.handler.detector_communication_katherine.model

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
enum class KatherineExecutionCommands(val internalID: Int, val commandID: Int) {

    SET_ACQ_MODE(200, 0x09),
    SET_NUMBER_OF_FRAMES(201, 0x13),
    MEASURE_READOUT_TEMPERATURE(202, 0x15),
    MEASURE_SENSOR_TEMPERATURE(203, 0x19),
    GET_READOUT_STATUS(204, 0x17),
    START_HW_COMMAND(205, 0x07),
    PERFORM_DIGITAL_TEST(206, 0x20),
    ACQ_SETUP(207, 0x21),
    ACQ_START(208, 0x03),
    ACQ_STOP(209, 0x06),
    UPLOAD_PIXEL_CONFIG(210, 0x12);


    companion object {

        fun getById(internalID: Int): KatherineExecutionCommands? {
            for (c in KatherineExecutionCommands.values()) {
                if (c.internalID == internalID) return c
            }
            return null
        }

        fun getByCommunicationId(internalID: Int): KatherineExecutionCommands? {
            for (c in KatherineExecutionCommands.values()) {
                if (c.commandID == internalID) return c
            }
            return null
        }
    }
}
