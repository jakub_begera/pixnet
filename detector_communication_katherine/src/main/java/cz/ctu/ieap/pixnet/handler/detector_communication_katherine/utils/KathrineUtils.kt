package cz.ctu.ieap.pixnet.handler.detector_communication_katherine.utils

import java.nio.ByteBuffer
import java.util.*

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
object KathrineUtils {

    fun bmcToMatrixConfig(bmc: ByteArray): ByteArray {
        assert(bmc.size == 65536)
        val buff = IntArray(16384)

        for (i in bmc.indices) {
            var y = i / 256
            val x = i % 256
            val tmp = bmc[i]
            y = 255 - y
            buff[64 * x + (y shr 2)] = buff[64 * x + (y shr 2)] or (tmp.toInt() shl 8 * (3 - y % 4))
        }

        val out = ByteArray(65536)
        var k = 0
        for (mem in buff) {
            val b = ByteBuffer.allocate(4).putInt(mem).array()
            for (i in 0..3) {
                out[k + 3 - i] = b[i]
            }
            k += 4
        }

        return out
    }

    fun encodeMatrixConfig(config: ByteArray): String {
        return Base64.getEncoder().encodeToString(config)
    }

    fun decodeMatrixConfig(string: String): ByteArray {
        return Base64.getDecoder().decode(string)
    }

}
