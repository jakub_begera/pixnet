package cz.ctu.ieap.pixnet.handler.detector_communication_katherine.service

import cz.ctu.ieap.pixnet.commons.exceptions.DetectorException
import cz.ctu.ieap.pixnet.commons.utils.EasyLogger
import cz.ctu.ieap.pixnet.handler.detector_communication_katherine.model.IncomingPacket

import java.io.IOException
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.SocketTimeoutException
import java.util.concurrent.ConcurrentLinkedQueue

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class PacketReceiver(private val socket: DatagramSocket, private val packetLength: Int, private val packetExpiration: Long) {
    private val logger = EasyLogger.get(this.javaClass)
    private var receiverThread: Thread? = null
    private var queueCleanerThread: Thread? = null
    private var isRunning: Boolean = false
    private val queue = ConcurrentLinkedQueue<IncomingPacket>()

    init {
        start()
    }

    private fun start() {
        val buf = ByteArray(packetLength)
        val datagramPacket = DatagramPacket(buf, buf.size)
        isRunning = true

        // receiver
        receiverThread = Thread {
            while (isRunning) {
                try {
                    socket.receive(datagramPacket)
                    val packet = IncomingPacket(datagramPacket.data, System.currentTimeMillis())
                    queue.add(packet)
                    logger.i("New packet - %s", packet.toString())
                } catch (e: SocketTimeoutException) {
                    logger.i("Receive timeout reach.")
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }
        }.apply { start() }

        // cleaner
        queueCleanerThread = Thread {
            while (isRunning) {
                queue.removeIf { packet -> packet.timestamp + packetExpiration < System.currentTimeMillis() }
                try {
                    Thread.sleep(packetExpiration * 10)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }

            }
        }.apply { start() }
    }

    fun stop() {
        isRunning = false
        receiverThread!!.interrupt()
    }

    @Throws(DetectorException::class)
    fun pollPacketFromQueue(commandId: Int, timeout: Long): IncomingPacket {
        val startTime = System.currentTimeMillis()
        while (startTime + timeout > System.currentTimeMillis()) {
            for (incomingPacket in queue) {
                if (incomingPacket.commandID == commandId) {
                    queue.remove(incomingPacket)
                    return incomingPacket
                }
            }
            try {
                Thread.sleep(100)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }

        }
        throw DetectorException("Polling packed from queue fail - Timeout")
    }
}
