package cz.ctu.ieap.pixnet.handler.detector_communication_katherine.model

import cz.ctu.ieap.pixnet.commons.exceptions.DetectorException
import cz.ctu.ieap.pixnet.commons.utils.ByteUtils
import cz.ctu.ieap.pixnet.commons.utils.ByteUtils.reverse
import java.util.*

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class IncomingPacket(buff: ByteArray, val timestamp: Long) {

    val commandID: Int
    val payload: ByteArray

    val intValue: Int
        get() = ByteUtils.bytesToInt(getBytes(0, 4))

    val floatValue: Float
        get() = ByteUtils.bytesToFloat(getBytes(0, 4))

    val longValueLsbMsb: ByteArray
        get() = getBytes(0, 4)

    init {
        assert(buff.size == 8)
        commandID = ByteUtils.bytesToInt(byteArrayOf(buff[7], buff[6]))
        this.payload = ByteArray(6)
        System.arraycopy(buff, 0, payload, 0, 6)
    }

    fun getIntValue(from: Int, to: Int): Int {
        assert(from < to && to - from <= 4)
        return ByteUtils.bytesToInt(getBytes(from, to))
    }

    fun getLongValue(from: Int, to: Int): Long {
        assert(from < to && to - from <= 8)
        return ByteUtils.bytesToLong(getBytes(from, to))
    }

    override fun toString(): String {
        return String.format("%d: %s", commandID, Arrays.toString(payload))
    }

    @Throws(DetectorException::class)
    fun checkAck() {
        for (i in payload.indices) {
            if (payload[i].toInt() != 0) throw DetectorException("Wrong ack payload - %s", Arrays.toString(payload))
        }
    }

    /**
     * Copies the specified range of the specified array into a new array and reverse it.
     * @param from the initial index of the range to be copied, inclusive
     * @param to the final index of the range to be copied, exclusive.
     * @return a new reversed array containing the specified range from the original array.
     */
    fun getBytes(from: Int, to: Int): ByteArray {
        return reverse(Arrays.copyOfRange(payload, from, to))
    }

    companion object {
        fun getLongFromLsbMsb(lsb: IncomingPacket, msb: IncomingPacket): Long {
            val arr = ByteArray(8)
            System.arraycopy(lsb.longValueLsbMsb, 0, arr, 4, 4)
            System.arraycopy(msb.longValueLsbMsb, 0, arr, 0, 4)
            return ByteUtils.bytesToLong(arr)
        }
    }

}
