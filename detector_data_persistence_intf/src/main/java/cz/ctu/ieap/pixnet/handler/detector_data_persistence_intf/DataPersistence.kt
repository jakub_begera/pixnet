package cz.ctu.ieap.pixnet.handler.detector_data_persistence_intf

import com.fasterxml.jackson.annotation.JsonIgnore
import cz.ctu.ieap.pixnet.commons.model.AbstractDataFrame

import java.util.concurrent.BlockingQueue

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
interface DataPersistence {

    fun setDataFrameQueue(queue: BlockingQueue<AbstractDataFrame>)

    fun start()

    fun stop()

    /**
     * @param config String configuration of the detector (YAML, JSON, ...)
     */
    @JsonIgnore
    fun setDetectorConfig(config: String)

    fun setCallback(callback: Callback)

    interface Callback {
        val classLoader: ClassLoader?
    }

}
