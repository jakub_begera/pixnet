package cz.ctu.ieap.pixnet.katherineEmulator

import cz.ctu.ieap.pixnet.commons.utils.ByteUtils
import cz.ctu.ieap.pixnet.katherineEmulator.model.AcqConfig
import cz.ctu.ieap.pixnet.katherineEmulator.model.EmulatorConfig
import cz.ctu.ieap.pixnet.kathrine_commons.model.DataFrameHeaders
import cz.ctu.ieap.pixnet.kathrine_commons.model.DetectorConfig
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress
import java.util.concurrent.TimeUnit

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class AcqController(private val clientAddress: InetAddress, private val clientPort: Int, val emulatorConfig: EmulatorConfig) {

    var acqJob: Job? = null
    private var dataSocket: DatagramSocket? = null
    private var acqConfig: AcqConfig? = null
    private var acqStartTimestamp: Long = 0

    fun startAcq(dataSocket: DatagramSocket, acqConfig: AcqConfig) {
        this.dataSocket = dataSocket
        this.acqConfig = acqConfig
        stopAcq()

        // validate acq config
        if (acqConfig.readoutMode == null) {
            System.err.println("Readout mode not given!")
            return
        }
        if (acqConfig.readoutMode == DetectorConfig.ReadOutMode.FRAME && acqConfig.numberOfFrames == null) {
            System.err.println("Number of frames not given (ist a mandatory parameter for FRAME based readout mode).")
            return
        }
        if (acqConfig.acqTime == null) {
            System.err.println("Acq time not give!")
            return
        }

        acqJob = launch {
            acqStartTimestamp = System.nanoTime()
            when (acqConfig.readoutMode) {
                DetectorConfig.ReadOutMode.DATA_DRIVEN -> {
                    val hits = performFrame(dataSocket, acqConfig)
                    println("Acq finished with $hits generated hits")
                }
                DetectorConfig.ReadOutMode.FRAME -> {
                    for (frame in 1..(acqConfig.numberOfFrames!!)) {
                        val hits = performFrame(dataSocket, acqConfig)
                        println("Frame $frame/${acqConfig.numberOfFrames!!} done with $hits generated hits")
                    }
                    println("Acq finished")
                }
            }
        }
    }

    fun stopAcq() {
        acqJob?.takeIf { it.isActive }?.apply {
            println("Stopping acq...")
            cancel()
            dataSocket?.send(MeasurementPacketBuilder(DataFrameHeaders.MEASUREMENT_ABORTED_NOTICE).build())
        }
    }

    private suspend fun performFrame(dataSocket: DatagramSocket, acqConfig: AcqConfig): Long {
        val startTS = System.nanoTime()
        dataSocket.send(MeasurementPacketBuilder(DataFrameHeaders.NEW_FRAME_ESTABLISHED).build())

        var eventsCount = 0L
        var toaOffset = 0L
        val acqTimeNS = acqConfig.acqTime!! * 10
        while (System.nanoTime() - startTS < acqTimeNS) {

            val elapsedTime = System.nanoTime() - startTS

            // compute ToA
            val toa = elapsedTime / 25 + 1
            val fastToa = (24 - (elapsedTime % 25) / 1.5625).toInt()
            val toaCoarse = (toa % 16384).toInt()

            // send ToA offset (Data-drive mode only)
            if (toa >= 16384 && acqConfig.readoutMode == DetectorConfig.ReadOutMode.DATA_DRIVEN) {
                val toaOffsetNew = toa / 16384
                if (toaOffset != toaOffsetNew) {
                    toaOffset = toaOffsetNew
                }
                val bytes = MeasurementPacketBuilder(DataFrameHeaders.PIXEL_TIMESTAMP_OFFSET)
                        .setData(toaOffset)
                        .build()
                dataSocket.send(bytes)
            }

            // generate new hit
            when (acqConfig.acqMode) {
                DetectorConfig.AcqMode.Fast_VCO_ON_TOA_TOT -> dataSocket.send(
                        MeasurementPacketBuilder(DataFrameHeaders.PIXEL_MEASUREMENT_DATA)
                                .setRandomCoordinates()
                                .setToA(toaCoarse)
                                .setFastToA(fastToa)
                                .setToTRandom()
                                .build())
                DetectorConfig.AcqMode.Fast_VCO_OFF_TOA_TOT -> dataSocket.send(
                        MeasurementPacketBuilder(DataFrameHeaders.PIXEL_MEASUREMENT_DATA)
                                .setRandomCoordinates()
                                .setToA(toaCoarse)
                                .setToTRandom()
                                .setHitCounterRandom()
                                .build())
                DetectorConfig.AcqMode.Fast_VCO_ON_TOA -> dataSocket.send(
                        MeasurementPacketBuilder(DataFrameHeaders.PIXEL_MEASUREMENT_DATA)
                                .setRandomCoordinates()
                                .setToA(toaCoarse)
                                .setFastToA(fastToa)
                                .build())
                DetectorConfig.AcqMode.Fast_VCO_OFF_TOA -> dataSocket.send(
                        MeasurementPacketBuilder(DataFrameHeaders.PIXEL_MEASUREMENT_DATA)
                                .setRandomCoordinates()
                                .setToA(toaCoarse)
                                .setHitCounterRandom()
                                .build())
                DetectorConfig.AcqMode.Fast_VCO_ON_EventCount_iToT -> dataSocket.send(
                        MeasurementPacketBuilder(DataFrameHeaders.PIXEL_MEASUREMENT_DATA)
                                .setRandomCoordinates()
                                .setiToT(1)
                                .setEventCounter(1)
                                .build())
                DetectorConfig.AcqMode.Fast_VCO_OFF_EventCount_iToT -> dataSocket.send(
                        MeasurementPacketBuilder(DataFrameHeaders.PIXEL_MEASUREMENT_DATA)
                                .setRandomCoordinates()
                                .setiToT(1)
                                .setEventCounter(1)
                                .setHitCounterRandom()
                                .build())
            }

            eventsCount++
            delay(((acqTimeNS.toDouble() / emulatorConfig.avgNumberOfEventsPerAcq.toDouble()) * 2 * Math.random()).toLong(),
                    TimeUnit.NANOSECONDS)

        }


        // send start of frame internal timestamp
        val frameStartTSInternal = startTS - acqStartTimestamp
        ByteUtils.getLsbFromLong(frameStartTSInternal).let {
            dataSocket.send(
                    MeasurementPacketBuilder(DataFrameHeaders.START_OF_FRAME_TIMESTAMP_LSB)
                            .setPayload(it)
                            .build())
        }
        ByteUtils.getMsbFromLong(frameStartTSInternal).let {
            dataSocket.send(
                    MeasurementPacketBuilder(DataFrameHeaders.START_OF_FRAME_TIMESTAMP_MSB)
                            .setPayload(it)
                            .build())
        }

        // send end of frame internal timestamp
        val frameEndTSInternal = System.nanoTime() - acqStartTimestamp
        ByteUtils.getLsbFromLong(frameEndTSInternal).let {
            dataSocket.send(
                    MeasurementPacketBuilder(DataFrameHeaders.END_OF_FRAME_TIMESTAMP_LSB)
                            .setPayload(it)
                            .build())
        }
        ByteUtils.getMsbFromLong(frameEndTSInternal).let {
            dataSocket.send(
                    MeasurementPacketBuilder(DataFrameHeaders.END_OF_FRAME_TIMESTAMP_MSB)
                            .setPayload(it)
                            .build())
        }

        dataSocket.send(MeasurementPacketBuilder(DataFrameHeaders.CURRENT_FRAME_FINISHED).setData(eventsCount).build())

        return eventsCount
    }

    private fun DatagramSocket.send(bytes: ByteArray) {
        send(DatagramPacket(bytes, bytes.size, clientAddress, clientPort))
    }

}


