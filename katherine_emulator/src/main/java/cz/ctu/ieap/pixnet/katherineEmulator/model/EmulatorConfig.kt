package cz.ctu.ieap.pixnet.katherineEmulator.model

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class EmulatorConfig {
    var portCommands: Int = 0
    var portData: Int = 0
    var host: String? = null
    var avgNumberOfEventsPerAcq: Int = 10
}
