package cz.ctu.ieap.pixnet.katherineEmulator

import cz.ctu.ieap.pixnet.commons.utils.ByteUtils
import cz.ctu.ieap.pixnet.kathrine_commons.model.DataFrameHeaders
import java.util.*
import kotlin.experimental.and

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class MeasurementPacketBuilder(private val header: DataFrameHeaders) {

    private val packet = BitSet(48)

    init {
        setValue(header.id, 44, 47)
    }

    fun build() = packet.toByteArray()

    fun setCoordinateX(value: Int) = setValue(value, 28, 35)
    fun setCoordinateY(value: Int) = setValue(value, 36, 43)
    fun setRandomCoordinates(): MeasurementPacketBuilder {
        setCoordinateX((Math.random() * 256).toInt())
        setCoordinateY((Math.random() * 256).toInt())
        return this
    }

    /**
     * Sets data on position 3..0
     * Accepts values from 0 to 15
     */
    fun setCol1(value: Int) = setValue(value, 0, 3)

    /**
     * Sets data on position 13..4
     * Accepts values from 0 to 1023
     */
    fun setCol2(value: Int) = setValue(value, 4, 13)

    /**
     * Sets data on position 27..14
     * Accepts values from 0 to 16383
     */
    fun setCol3(value: Int) = setValue(value, 14, 27)

    fun setToA(value: Int) = setCol3(value)
    fun setiToT(value: Int) = setCol3(value)
    fun setToT(value: Int) = setCol2(value)
    fun setEventCounter(value: Int) = setCol2(value)
    fun setFastToA(value: Int) = setCol1(value)
    fun setHitCounter(value: Int) = setCol1(value)

//    fun setToTRandom() = setToT((Math.random() * 1024).toInt())
    fun setToTRandom() = setToT(3)
//    fun setHitCounterRandom() = setHitCounter((Math.random() * 16).toInt())
    fun setHitCounterRandom() = setHitCounter(2)

    /**
     * Sets data on position 0..43
     * Accepts values from 0 to 17_592_186_044_416
     */
    fun setData(value: Long) = setValue(value, 0, 43)

    fun setPayload(data: ByteArray) = setData(ByteUtils.bytesToLong(data))

    private fun setValue(value: Int, biteFrom: Int, biteTo: Int) = setValue(value.toLong(), biteFrom, biteTo)

    private fun setValue(value: Long, biteFrom: Int, biteTo: Int): MeasurementPacketBuilder {
        assert(biteTo - biteFrom <= 32)
        assert(biteTo <= 47 && biteFrom >= 0)
        val bytes = ByteUtils.longToBytes(value)

        for (i in biteFrom..biteTo) {
            val position = i - biteFrom

            val byteIndex = bytes.size - 1 - position / 8
            val biteIndexInByte = position % 8

            packet[i] = bytes[byteIndex].isBitSet(biteIndexInByte)

        }

        return this
    }


}

fun Byte.isBitSet(bit: Int): Boolean {
    return (this and ((1 shl bit).toByte())).toInt() != 0
}