package cz.ctu.ieap.pixnet.katherineEmulator

import cz.ctu.ieap.pixnet.handler.detector_communication_katherine.model.IncomingPacket
import cz.ctu.ieap.pixnet.handler.detector_communication_katherine.model.KatherineExecutionCommands
import cz.ctu.ieap.pixnet.handler.detector_communication_katherine.model.KatherineValueCommands
import cz.ctu.ieap.pixnet.handler.detector_communication_katherine.utils.PacketBuilder
import cz.ctu.ieap.pixnet.katherineEmulator.model.AcqConfig
import cz.ctu.ieap.pixnet.katherineEmulator.model.EmulatorConfig
import cz.ctu.ieap.pixnet.kathrine_commons.model.DetectorConfig
import cz.ctu.ieap.pixnet.kathrine_commons.utils.LsbMsbPacketMatcher
import java.io.IOException
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class KatherineEmulator(val emulatorConfig: EmulatorConfig) {

    private var isRunning: Boolean = false
    private val commSocket: DatagramSocket
    private val dataSocket: DatagramSocket
    private val acqConfig = AcqConfig()
    private var acqController: AcqController? = null

    private val lsbMsbPacketMatcher = LsbMsbPacketMatcher<KeysLsbMsbPackets, IncomingPacket>()

    enum class KeysLsbMsbPackets {
        START_OF_FRAME_TIMESTAMP,
        END_OF_FRAME_TIMESTAMP,
        ACQ_TIME
    }

    init {
        commSocket = DatagramSocket(emulatorConfig.portCommands)
        dataSocket = DatagramSocket()
    }

    @Throws(IOException::class)
    fun start() {
        isRunning = true
        startReceiverThread()
    }

    @Throws(IOException::class)
    private fun startReceiverThread() {
        while (isRunning) {
            val buf = ByteArray(8)
            val packet = DatagramPacket(buf, buf.size)
            commSocket.receive(packet)

            val address = packet.address
            val port = packet.port

            val response = handleCommandPacket(IncomingPacket(buf, System.currentTimeMillis()), address, port)
            val responsePacket = DatagramPacket(response, response.size, address, port)
            commSocket.send(responsePacket)
        }
    }

    private fun handleCommandPacket(packet: IncomingPacket, address: InetAddress, port: Int): ByteArray {
        println("New command packet occurred: $packet")
        val valueCommand = KatherineValueCommands.getByCommunicationId(packet.commandID)
        if (valueCommand != null) {
            return processValueCommand(valueCommand, packet)
        }
        val executionCommand = KatherineExecutionCommands.getByCommunicationId(packet.commandID)
        return if (executionCommand != null) {
            processExecutionCommand(executionCommand, packet, address, port)
        } else buildAcqPacket(packet.commandID)
    }


    private fun processValueCommand(command: KatherineValueCommands, packet: IncomingPacket) = when (packet.commandID) {

        // ACQ time
        KatherineValueCommands.ACQUISITION_TIME.setterCommandIdLSB -> {
            lsbMsbPacketMatcher.putLsbAndCheck(KeysLsbMsbPackets.ACQ_TIME, packet)?.let {
                acqConfig.acqTime = IncomingPacket.getLongFromLsbMsb(it.first, it.second).apply {
                    println("Setting acq time: ${this.toDouble() / 1E8} s (LSB came as second)")
                }

            }
            buildAcqPacket(packet.commandID)
        }
        KatherineValueCommands.ACQUISITION_TIME.setterCommandIdMSB -> {
            lsbMsbPacketMatcher.putMsbAndCheck(KeysLsbMsbPackets.ACQ_TIME, packet)?.let {
                acqConfig.acqTime = IncomingPacket.getLongFromLsbMsb(it.first, it.second).apply {
                    println("Setting acq time: ${this.toDouble() / 1E-8} s (MSB came as second)")
                }
            }
            buildAcqPacket(packet.commandID)
        }

        // BIAS
        KatherineValueCommands.BIAS.setterCommandId -> {
            acqConfig.bias = packet.floatValue.apply {
                println("Setting bias: $this V")
            }
            buildAcqPacket(packet.commandID)
        }
        KatherineValueCommands.BIAS.getterCommandId -> {
            println("Generating bias: ${acqConfig.bias ?: 0f} V")
            PacketBuilder.builder().commandID(packet.commandID).putFloat(acqConfig.bias ?: 0f).build()
        }

        // Chip ID
        KatherineValueCommands.CHIP_ID.getterCommandId -> {
            println("Generating chip ID")
            PacketBuilder.builder().commandID(packet.commandID).putInt(10).build()
        }

        // command not implemented
        else -> {
            println("Value command ${command.name} is not implemented. Acq packet sent instead.")
            buildAcqPacket(packet.commandID)
        }

    }

    private fun processExecutionCommand(command: KatherineExecutionCommands, packet: IncomingPacket,
                                        address: InetAddress, port: Int) = when (command) {

        KatherineExecutionCommands.SET_ACQ_MODE -> {
            val fastVCOEnabled = packet.intValue shr 8 and 1 == 1
            val mode = packet.intValue and 0b11111111
            acqConfig.acqMode = DetectorConfig.AcqMode.find(mode, fastVCOEnabled)
            acqConfig.acqMode?.let {
                println("Setting acq mode to ${it.mode}")
            } ?: println("Acq mode not recognized")
            buildAcqPacket(packet.commandID)
        }
        KatherineExecutionCommands.SET_NUMBER_OF_FRAMES -> {
            acqConfig.numberOfFrames = packet.intValue.apply {
                println("Setting number of frames: $this")
            }
            buildAcqPacket(packet.commandID)
        }
        KatherineExecutionCommands.MEASURE_READOUT_TEMPERATURE -> {
            PacketBuilder.builder()
                    .commandID(packet.commandID)
                    .putFloat((70f + Math.random().toFloat() * 10f).apply {
                        println("Generating readout temperature: ${acqConfig.bias ?: 0f} °C")
                    })
                    .build()
        }
        KatherineExecutionCommands.MEASURE_SENSOR_TEMPERATURE -> {
            PacketBuilder.builder()
                    .commandID(packet.commandID)
                    .putFloat((78f + Math.random().toFloat() * 10f).apply {
                        println("Generating sensor temperature: ${acqConfig.bias ?: 0f} °C")
                    })
                    .build()
        }
        KatherineExecutionCommands.PERFORM_DIGITAL_TEST -> {
            println("Performing digital test... result OK")
            PacketBuilder.builder()
                    .commandID(packet.commandID)
                    .putInt(64)
                    .build()
        }
        KatherineExecutionCommands.ACQ_START -> {
            acqConfig.readoutMode = DetectorConfig.ReadOutMode.byValue(packet.intValue)
            acqConfig.readoutMode?.let {
                println("Setting readout mode ${it.mode}")
                println("Starting acq...")
                acqController?.stopAcq()
                acqController = AcqController(address, emulatorConfig.portData, emulatorConfig).apply {
                    startAcq(dataSocket, acqConfig.copy())
                }
            } ?: println("Readout not recognized. Acquisition will not start...")
            buildAcqPacket(packet.commandID)
        }
        KatherineExecutionCommands.ACQ_STOP -> {
            println("Stopping acq...")
            acqController?.stopAcq()
            buildAcqPacket(packet.commandID)
        }

        // command not implemented
        else -> {
            println("Execution command ${command.name} is not implemented. Acq packet sent instead.")
            buildAcqPacket(packet.commandID)
        }
    }

    private fun buildAcqPacket(commandId: Int): ByteArray {
        return PacketBuilder.builder()
                .commandID(commandId)
                .build()
    }

}
