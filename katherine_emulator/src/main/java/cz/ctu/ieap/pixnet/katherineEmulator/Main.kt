package cz.ctu.ieap.pixnet.katherineEmulator

import cz.ctu.ieap.pixnet.katherineEmulator.model.EmulatorConfig
import org.yaml.snakeyaml.Yaml
import java.io.FileInputStream

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
object Main {

    @JvmStatic
    fun main(args: Array<String>) {
        if (args.isEmpty()) {
            System.err.println("Not enough arguments given. Expected:\n[0] YAML config file")
            System.exit(1)
        }
        println(KatherineEmulator::class.java.name)
        val yaml = Yaml()
        val emulatorConfig = yaml.loadAs(FileInputStream(args[0]), EmulatorConfig::class.java)
        val katherineEmulator = KatherineEmulator(emulatorConfig)
        println("Katherine emulator has started with following configuration:\n" +
                "Commands port: ${emulatorConfig.portCommands}\n" +
                "Data port: ${emulatorConfig.portData}"
        )
        katherineEmulator.start()

    }

}
