package cz.ctu.ieap.pixnet.katherineEmulator.model

import cz.ctu.ieap.pixnet.kathrine_commons.model.DetectorConfig

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
data class AcqConfig(
        var bias: Float? = null,
        var acqTime: Long? = null,
        var acqMode: DetectorConfig.AcqMode? = null,
        var readoutMode: DetectorConfig.ReadOutMode? = null,
        var numberOfFrames: Int? = null
)