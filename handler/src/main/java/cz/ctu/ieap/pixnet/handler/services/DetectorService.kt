package cz.ctu.ieap.pixnet.handler.services

import cz.ctu.ieap.pixnet.commons.exceptions.DetectorException
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.model.ValuePayload
import cz.ctu.ieap.pixnet.handler.model.Detector
import cz.ctu.ieap.pixnet.handler.model.Status
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
@Service
class DetectorService {

    private val logger = LoggerFactory.getLogger(this.javaClass)

    /**
     * Connect detectors
     *
     * @param detectors list of $[Detector] to connect.
     * @return number of successfully connected detectors.
     */
    fun connectMore(vararg detectors: Detector): Int {
        var connectedDetectors = 0
        for (d in detectors) {
            try {
                connect(d)
                logger.info("Detector %s successfully connected", d.toString())
                connectedDetectors++
            } catch (e: DetectorException) {
                logger.warn(String.format("Detector %s connection fail", d.toString()), e)
            }

        }
        return connectedDetectors
    }

    @Throws(DetectorException::class)
    fun connect(detector: Detector) {
        detector.detectorComm.connect()
        detector.status.connection = Status.Connection.ONLINE
    }

    /**
     * Disconnect detectors
     *
     * @param detectors list of $[Detector] to disconnect.
     * @return number of successfully connected detectors.
     */
    fun disconnectMore(vararg detectors: Detector): Int {
        var disconnectedDetectors = 0
        for (d in detectors) {
            try {
                disconnect(d)
                logger.info("Detector %s successfully disconnected", d.toString())
                disconnectedDetectors++
            } catch (e: DetectorException) {
                logger.warn(String.format("Detector %s disconnection fail", d.toString()), e)
            }

        }
        return disconnectedDetectors
    }

    @Throws(DetectorException::class)
    fun disconnect(detector: Detector) {
        detector.dataPersistence.stop()
        detector.detectorComm.disconnect()
        detector.status.connection = Status.Connection.OFFLINE
    }

    @Throws(DetectorException::class)
    fun executeSetValueCommand(detector: Detector, commandID: Int, payload: ValuePayload) {
        detector.detectorComm.executeSetValueCommand(commandID, payload)
    }

    @Throws(DetectorException::class)
    fun executeGetValueCommand(detector: Detector, commandID: Int): ValuePayload {
        return detector.detectorComm.executeGetValueCommand(commandID)
    }

    @Throws(DetectorException::class)
    fun executeExecutionCommand(detector: Detector, commandID: Int, input: Map<String, ValuePayload>): Map<String, ValuePayload> {
        return detector.detectorComm.executeExecutionCommand(commandID, input)
    }

    @Throws(DetectorException::class)
    fun uploadFile(detector: Detector, fileKey: String, file: ByteArray) {
        detector.detectorComm.uploadFile(fileKey, file)
    }
}
