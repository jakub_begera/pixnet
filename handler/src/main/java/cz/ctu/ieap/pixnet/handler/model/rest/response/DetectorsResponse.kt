package cz.ctu.ieap.pixnet.handler.model.rest.response

import cz.ctu.ieap.pixnet.handler.model.Detector

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class DetectorsResponse : BaseResponse {

    val detectors: List<Detector>

    constructor(success: Boolean, detectors: List<Detector>) : super(success) {
        this.detectors = detectors
    }

    constructor(success: Boolean, message: String, detectors: List<Detector>) : super(success, message) {
        this.detectors = detectors
    }
}
