package cz.ctu.ieap.pixnet.handler.config

import cz.ctu.ieap.pixnet.handler.model.config.ExternalConfig
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.thymeleaf.spring5.SpringTemplateEngine
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver
import org.thymeleaf.spring5.view.ThymeleafViewResolver
import org.yaml.snakeyaml.Yaml

import java.io.FileInputStream
import java.io.FileNotFoundException

@Configuration
@EnableWebMvc
class ApplicationConfiguration {

    @Bean
    fun externalConfiguration(): ExternalConfig? {
        val yaml = Yaml()
        var config: ExternalConfig? = null
        try {
            config = yaml.loadAs(FileInputStream(System.getProperties().getProperty(
                    "externalConfigPath")), ExternalConfig::class.java)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }

        return config
    }

    @Bean
    fun templateEngine(): SpringTemplateEngine {
        val templateEngine = SpringTemplateEngine()
        templateEngine.setTemplateResolver(thymeleafTemplateResolver())
        return templateEngine
    }

    @Bean
    fun thymeleafTemplateResolver(): SpringResourceTemplateResolver {
        val templateResolver = SpringResourceTemplateResolver()
        templateResolver.prefix = "classpath:WEB-INF/views/"
        templateResolver.suffix = ".html"
        templateResolver.setTemplateMode("HTML")
        return templateResolver
    }

    @Bean
    fun thymeleafViewResolver(): ThymeleafViewResolver {
        val viewResolver = ThymeleafViewResolver()
        viewResolver.templateEngine = templateEngine()
        return viewResolver
    }

}