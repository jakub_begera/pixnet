package cz.ctu.ieap.pixnet.handler.model.config

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class ExternalConfig {

    var portToListen: Int = 0
    var handlerName: String? = null
}
