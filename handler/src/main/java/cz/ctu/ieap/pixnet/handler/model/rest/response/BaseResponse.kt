package cz.ctu.ieap.pixnet.handler.model.rest.response

import com.fasterxml.jackson.annotation.JsonInclude

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
open class BaseResponse {

    var isSuccess: Boolean = false
        private set

    @JsonInclude(JsonInclude.Include.NON_NULL)
    var message: String? = null

    constructor()

    constructor(success: Boolean) {
        this.isSuccess = success
    }

    constructor(success: Boolean, message: String) {
        this.isSuccess = success
        this.message = message
    }

    constructor(success: Boolean, message: String, vararg args: Any?) {
        this.isSuccess = success
        this.message = String.format(message, *args)
    }
}
