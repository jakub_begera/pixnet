package cz.ctu.ieap.pixnet.handler.model.rest.response

import cz.ctu.ieap.pixnet.handler.detector_communication_intf.model.ValuePayload

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class GetValueResponse : BaseResponse {

    var payload: ValuePayload? = null

    constructor(success: Boolean, payload: ValuePayload) : super(success) {
        this.payload = payload
    }

    constructor(success: Boolean, payload: ValuePayload, message: String, vararg args: Any) : super(success, message, *args) {
        this.payload = payload
    }
}
