package cz.ctu.ieap.pixnet.handler.model

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class Status {

    var connection = Connection.UNKNOWN
    var measurement = Measurement.UNKNOWN

    enum class Connection private constructor(val nameProperty: String) {
        UNKNOWN("unknown"),
        ONLINE("online"),
        OFFLINE("offline"),
        CONNECTING("connecting")
    }

    enum class Measurement private constructor(val nameProperty: String) {
        UNKNOWN("unknown"),
        IDLE("online"),
        MEASURING("unknown")
    }

}
