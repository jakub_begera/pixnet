package cz.ctu.ieap.pixnet.handler.controller

import cz.ctu.ieap.pixnet.commons.exceptions.DetectorException
import cz.ctu.ieap.pixnet.commons.utils.EasyLogger
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.DetectorComm
import cz.ctu.ieap.pixnet.handler.detector_communication_katherine.CommImpl
import cz.ctu.ieap.pixnet.handler.detector_data_persistence_intf.DataPersistence
import cz.ctu.ieap.pixnet.handler.model.rest.request.ExecuteExecutionCommandRequest
import cz.ctu.ieap.pixnet.handler.model.rest.request.ExecuteValueCommandRequest
import cz.ctu.ieap.pixnet.handler.model.rest.response.BaseResponse
import cz.ctu.ieap.pixnet.handler.model.rest.response.DetectorsResponse
import cz.ctu.ieap.pixnet.handler.model.rest.response.ExecutionCommandResponse
import cz.ctu.ieap.pixnet.handler.model.rest.response.GetValueResponse
import cz.ctu.ieap.pixnet.handler.services.DetectorService
import cz.ctu.ieap.pixnet.handler.services.DetectorsManager
import cz.ctu.ieap.pixnet.handler.utils.PluginLoader
import cz.ctu.ieap.pixnet.katherine.persistence.file.DataPersistenceFile
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import java.io.IOException
import java.nio.charset.Charset

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
@RestController
@RequestMapping("/api/detector")
class DetectorsRestController {

    private val logger = EasyLogger.get(this.javaClass)

    @Autowired
    private lateinit var detectorService: DetectorService
    @Autowired
    private lateinit var detectorsManager: DetectorsManager

    val allDetectors: ResponseEntity<*>
        @GetMapping("/getAll")
        get() = ResponseEntity.ok(DetectorsResponse(true, detectorsManager.allDetectors))

    @PostMapping("/add")
    fun addDetector(@RequestParam(name = "id") id: String,
                    @RequestParam(name = "config", required = false) config: MultipartFile?,
                    @RequestParam(name = "plugin_comm", required = false) pluginComm: MultipartFile?,
                    @RequestParam(name = "plugin_data_persistence", required = false) pluginDataPersistence: MultipartFile?): ResponseEntity<*> {

        // Plugins jars
        val pluginLoader = PluginLoader<DetectorComm, DataPersistence>()
        pluginLoader.init(pluginComm?.bytes, pluginDataPersistence?.bytes)

        // communication plugin
        val detectorComm: DetectorComm
        detectorComm = if (pluginComm != null) {
            try {
                pluginLoader.getPlugin1()
            } catch (e: Exception) {
                logger.e(e, "Unable to load detector communication interface")
                return ResponseEntity(BaseResponse(true, "Unable to load detector communication interface - %s",
                        e.message), null, HttpStatus.BAD_REQUEST)
            }

        } else {
            CommImpl()
        }

        // data persistence plugin
        val dataPersistence = if (pluginDataPersistence != null) {
            try {
                pluginLoader.getPlugin2()
            } catch (e: Exception) {
                logger.e(e, "Unable to load detector data persistence interface")
                return ResponseEntity(BaseResponse(true, "Unable to load detector data persistence interface - %s",
                        e.message), null, HttpStatus.BAD_REQUEST)
            }

        } else {
            DataPersistenceFile()
        }

        val detector = detectorsManager.addDetector(
                id,
                detectorComm,
                pluginLoader.classLoader,
                dataPersistence
        )

        // comm plugin callback
        detectorComm.setCallback { detector.pluginsClassLoader }

        // data persistence plugin callback
        dataPersistence.setCallback(object : DataPersistence.Callback {
            override val classLoader: ClassLoader?
                get() = detector.pluginsClassLoader
        })


        // configuration
        if (config != null) {
            try {
                val yamlConfig = String(config.bytes, Charset.forName("UTF-8"))
                detectorComm.detectorConfig = yamlConfig
                dataPersistence.setDetectorConfig(yamlConfig)
            } catch (e: Exception) {
                logger.e(e, "Broken config")
                return ResponseEntity(BaseResponse(true, "Broken config - %s", e.message), null, HttpStatus.BAD_REQUEST)
            }

        }

        // connect plugins together
        dataPersistence.setDataFrameQueue(detectorComm.dataFrameQueue)
        dataPersistence.start()

        return ResponseEntity(BaseResponse(true), null, HttpStatus.OK)
    }

    @DeleteMapping("/remove")
    fun removeDetector(@RequestParam(name = "id") id: String): ResponseEntity<*> {
        val detector = detectorsManager.getDetector(id)
        if (detector == null) {
            logger.w("No detector found with id %s", id)
            return ResponseEntity(BaseResponse(false, "No detector found with id %s",
                    id), null, HttpStatus.NOT_FOUND)
        }
        return try {
            detectorService.disconnect(detector)
            detectorsManager.removeDetector(id)
            ResponseEntity.ok(BaseResponse(true))
        } catch (e: DetectorException) {
            val message = String.format("Unable to disconnect from detector - %s", e.message)
            logger.w(message, e)
            ResponseEntity(BaseResponse(false,
                    message), null, HttpStatus.BAD_REQUEST)
        }

    }

    @PostMapping("/connect")
    fun connect(@RequestParam(name = "id") id: String): ResponseEntity<*> {
        val detector = detectorsManager.getDetector(id)
        if (detector == null) {
            logger.w("No detector found with id %s", id)
            return ResponseEntity(BaseResponse(false, "No detector found with id %s",
                    id), null, HttpStatus.NOT_FOUND)
        }
        return try {
            detectorService.connect(detector)
            ResponseEntity.ok(BaseResponse(true))
        } catch (e: DetectorException) {
            val message = String.format("Unable to connect to detector - %s", e.message)
            logger.w(e, message)
            ResponseEntity(BaseResponse(false,
                    message), null, HttpStatus.BAD_REQUEST)
        }

    }

    @PostMapping("/disconnect")
    fun disconnect(@RequestParam(name = "id") id: String): ResponseEntity<*> {
        val detector = detectorsManager.getDetector(id)
        if (detector == null) {
            logger.w("No detector found with id %s", id)
            return ResponseEntity(BaseResponse(false, "No detector found with id %s",
                    id), null, HttpStatus.NOT_FOUND)
        }
        return try {
            detectorService.disconnect(detector)
            ResponseEntity.ok(BaseResponse(true))
        } catch (e: DetectorException) {
            val message = String.format("Unable to disconnect from detector - %s", e.message)
            logger.w(e, message)
            ResponseEntity(BaseResponse(false,
                    message), null, HttpStatus.BAD_REQUEST)
        }

    }

    @PostMapping("/executeValueCommand")
    fun executeValueCommand(@RequestBody request: ExecuteValueCommandRequest): ResponseEntity<*> {
        val detector = detectorsManager.getDetector(request.detectorID!!)
        if (detector == null) {
            logger.w("No detector found with id %s", request.detectorID.toString())
            return ResponseEntity(BaseResponse(false, "No detector found with id %s",
                    request.detectorID), null, HttpStatus.NOT_FOUND)
        }
        if (request.operation == null) {
            return ResponseEntity(BaseResponse(false,
                    "Operation (GET/SET) is missing!"), null, HttpStatus.METHOD_NOT_ALLOWED)
        }
        when (request.operation) {
            ExecuteValueCommandRequest.Operation.GET -> {
                return try {
                    val payload = detectorService.executeGetValueCommand(detector, request.commandID)
                    ResponseEntity.ok(GetValueResponse(true, payload))
                } catch (e: DetectorException) {
                    logger.w(e, "Unable to get value from detector")
                    ResponseEntity(BaseResponse(false,
                            "Unable to get value from detector - %s", e.message), null, HttpStatus.BAD_REQUEST)
                }
            }
            ExecuteValueCommandRequest.Operation.SET -> {
                return try {
                    detectorService.executeSetValueCommand(detector, request.commandID, request.payload!!)
                    ResponseEntity.ok(BaseResponse(true))
                } catch (e: DetectorException) {
                    logger.w(e, "Unable to set value to detector")
                    ResponseEntity(BaseResponse(false, "Unable to set value to detector (%s) - %s", request.toString(), e.message), null, HttpStatus.BAD_REQUEST)
                }
            }
            else -> return ResponseEntity(BaseResponse(false, "Operation %s not allowed!", request.toString(), request.operation), null, HttpStatus.METHOD_NOT_ALLOWED)
        }
    }

    @PostMapping("/executeExecutionCommand")
    fun executeExecutionCommand(@RequestBody request: ExecuteExecutionCommandRequest): ResponseEntity<*> {
        val detector = detectorsManager.getDetector(request.detectorID!!)
        if (detector == null) {
            logger.w("No detector found with id %s", request.detectorID.toString())
            return ResponseEntity(BaseResponse(false, "No detector found with id %s",
                    request.detectorID), null, HttpStatus.NOT_FOUND)
        }

        return try {
            val output = detectorService.executeExecutionCommand(detector, request.commandID, request.input!!)
            ResponseEntity.ok(ExecutionCommandResponse(true, output))
        } catch (e: DetectorException) {
            logger.w(e, "Unable to execute command - %s", e.message.toString())
            ResponseEntity(BaseResponse(false, "Unable to execute command - %s", e.message), null, HttpStatus.NOT_FOUND)
        }

    }

    @PostMapping("/uploadFile")
    fun uploadFile(@RequestParam(name = "detectorID") detectorID: String,
                   @RequestParam(name = "fileID") fileID: String,
                   @RequestParam file: MultipartFile): ResponseEntity<*> {
        val detector = detectorsManager.getDetector(detectorID)
        if (detector == null) {
            logger.w("No detector found with id %s", detectorID)
            return ResponseEntity(BaseResponse(false, "No detector found with id %s",
                    detectorID), null, HttpStatus.NOT_FOUND)
        }

        try {
            detectorService.uploadFile(detector, fileID, file.bytes)
        } catch (e: IOException) {
            logger.w(e, "File uploading fail.")
            return ResponseEntity(BaseResponse(false, "File uploading fail - %s",
                    e.message), null, HttpStatus.BAD_REQUEST)
        } catch (e: DetectorException) {
            logger.w(e, "File uploading fail.")
            return ResponseEntity(BaseResponse(false, "File uploading fail - %s", e.message), null, HttpStatus.BAD_REQUEST)
        }

        return ResponseEntity.ok(BaseResponse(true))
    }

}
