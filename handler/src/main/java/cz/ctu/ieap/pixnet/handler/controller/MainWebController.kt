package cz.ctu.ieap.pixnet.handler.controller

import cz.ctu.ieap.pixnet.handler.Application.Companion.APP_VERSION
import cz.ctu.ieap.pixnet.handler.model.config.ExternalConfig
import cz.ctu.ieap.pixnet.handler.services.DetectorsManager
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping


/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
@Controller
class MainWebController {

    @Autowired
    private val detectorsManager: DetectorsManager? = null
    @Autowired
    private val externalConfig: ExternalConfig? = null
    private val logger = LoggerFactory.getLogger(this.javaClass)

    @RequestMapping("/")
    fun index(): String {
        return "redirect:/detectors"
    }

    @RequestMapping("/detectors")
    fun detectors(model: Model): String {
        addHeaderAndFooterAttributes(model, "detectors")
        model.addAttribute("detectors", detectorsManager!!.allDetectors)
        return "detectors"
    }

    @RequestMapping("/about")
    fun about(model: Model): String {
        addHeaderAndFooterAttributes(model, "about")
        return "about"
    }

    private fun addHeaderAndFooterAttributes(model: Model, module: String) {
        model.addAttribute("module", module)
        model.addAttribute("handlerName", externalConfig!!.handlerName)
        model.addAttribute("appVersion", APP_VERSION)
    }
}
