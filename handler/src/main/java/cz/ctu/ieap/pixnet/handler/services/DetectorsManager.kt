package cz.ctu.ieap.pixnet.handler.services

import cz.ctu.ieap.pixnet.handler.detector_communication_intf.DetectorComm
import cz.ctu.ieap.pixnet.handler.detector_data_persistence_intf.DataPersistence
import cz.ctu.ieap.pixnet.handler.model.Detector
import org.springframework.stereotype.Service
import java.util.*

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
@Service
class DetectorsManager {

    private val detectors = HashMap<String, Detector>()

    /**
     * Get list of all detectors, sorted by their ID.
     * @return list of detectors.
     */
    val allDetectors: List<Detector>
        get() {
            val detectors = ArrayList(this.detectors.values)
            detectors.sortBy { it.detectorID }
            return detectors
        }

    fun addDetector(id: String, detectorComm: DetectorComm,
                    pluginsClassLoader: ClassLoader?,
                    persistenceImpl: DataPersistence): Detector {
        val detector = Detector(id, detectorComm, pluginsClassLoader, persistenceImpl)
        detectors[detector.detectorID] = detector
        return detector
    }

    fun getDetector(id: String): Detector? {
        return detectors[id]
    }

    /**
     * Removes the detector for the specified id.
     * @param id detector id
     * @return the previous detector associated with <tt>id</tt>,
     * or <tt>null</tt> if there was no detector for <tt>id</tt>.
     */
    fun removeDetector(id: String): Detector? {
        return detectors.remove(id)
    }

}
