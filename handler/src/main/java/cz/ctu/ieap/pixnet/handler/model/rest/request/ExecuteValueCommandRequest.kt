package cz.ctu.ieap.pixnet.handler.model.rest.request

import cz.ctu.ieap.pixnet.handler.detector_communication_intf.model.ValuePayload

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class ExecuteValueCommandRequest {

    var detectorID: String? = null
    var commandID: Int = 0
    var operation: Operation? = null
    var payload: ValuePayload? = null

    override fun toString(): String {
        return String.format("%s: %05X, %s", operation!!.name, commandID and 0xFFFFF, payload!!.toString())
    }

    enum class Operation {
        GET,
        SET
    }
}
