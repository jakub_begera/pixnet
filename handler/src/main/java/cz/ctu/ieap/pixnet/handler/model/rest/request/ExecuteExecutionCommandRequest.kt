package cz.ctu.ieap.pixnet.handler.model.rest.request

import cz.ctu.ieap.pixnet.handler.detector_communication_intf.model.ValuePayload

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class ExecuteExecutionCommandRequest {

    var detectorID: String? = null
    var commandID: Int = 0
    var input: Map<String, ValuePayload>? = null

}
