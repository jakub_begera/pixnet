package cz.ctu.ieap.pixnet.handler.model.rest.response

import cz.ctu.ieap.pixnet.handler.detector_communication_intf.model.ValuePayload

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class ExecutionCommandResponse : BaseResponse {

    var output: Map<String, ValuePayload>? = null

    constructor() {}

    constructor(success: Boolean, output: Map<String, ValuePayload>) : super(success) {
        this.output = output
    }
}
