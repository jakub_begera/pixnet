package cz.ctu.ieap.pixnet.handler.utils

import cz.ctu.ieap.pixnet.commons.exceptions.DetectorException
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.net.URLClassLoader
import java.util.jar.JarFile

@Suppress("UNCHECKED_CAST")
/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class PluginLoader<T1, T2> {

    var classLoader: URLClassLoader? = null
        private set
    private var plugin1: T1? = null
    private var plugin2: T2? = null

    fun init(vararg jars: ByteArray?) {
        val jarFiles = mutableListOf<File>()

        jars.forEach { jar ->
            jar?.let {
                val temp = File.createTempFile(String.format("temp_plugin_%d", System.currentTimeMillis()), ".jar")
                temp.deleteOnExit()
                val outputStream = BufferedOutputStream(FileOutputStream(temp))
                outputStream.write(jar, 0, jar.size)
                outputStream.flush()
                outputStream.close()
                jarFiles.add(temp)
            }
        }

        val jarsURI = jarFiles.map { it.toURI().toURL() }
        classLoader = URLClassLoader(jarsURI.toTypedArray(), PluginLoader::class.java.classLoader)

        jarFiles.getOrNull(0)?.let {
            val jarFile = JarFile(it)
            val pluginImplClass = jarFile.manifest.mainAttributes.getValue("PluginImplClass")
            val impl = Class.forName(pluginImplClass, true, classLoader)
            plugin1 = impl.getConstructor().newInstance() as? T1?
        }

        jarFiles.getOrNull(1)?.let {
            val jarFile = JarFile(it)
            val pluginImplClass = jarFile.manifest.mainAttributes.getValue("PluginImplClass")
            val impl = Class.forName(pluginImplClass, true, classLoader)
            plugin2 = impl.getConstructor().newInstance() as? T2?
        }

    }

    fun getPlugin1(): T1 {
        return plugin1 ?: throw DetectorException("Plugin 1 is not initialized")
    }

    fun getPlugin2(): T2 {
        return plugin2 ?: throw DetectorException("Plugin 2 is not initialized")
    }
}
