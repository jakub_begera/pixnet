package cz.ctu.ieap.pixnet.handler.model

import com.fasterxml.jackson.annotation.JsonIgnore
import cz.ctu.ieap.pixnet.handler.detector_communication_intf.DetectorComm
import cz.ctu.ieap.pixnet.handler.detector_data_persistence_intf.DataPersistence


/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class Detector(
        /**
         * Unique identifier of the detector.
         */
        val detectorID: String, val detectorComm: DetectorComm,
        @field:JsonIgnore
        val pluginsClassLoader: ClassLoader?,
        @field:JsonIgnore
        val dataPersistence: DataPersistence) {

    val status = Status()

    override fun toString(): String {
        return detectorID
    }
}
