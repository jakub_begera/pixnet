package cz.ctu.ieap.pixnet.handler.controller

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders

import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class MainRestControllerTest {

    @Autowired
    private val mvc: MockMvc? = null


    @Test
    fun detectors() {
        mvc!!.perform(MockMvcRequestBuilders.get("/api/detector/getAll").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
    }

    companion object {

        init {
            System.getProperties()["externalConfigPath"] = "test_config.yaml"
        }
    }
}