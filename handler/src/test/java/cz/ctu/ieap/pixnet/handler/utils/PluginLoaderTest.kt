package cz.ctu.ieap.pixnet.handler.utils

import cz.ctu.ieap.pixnet.handler.detector_communication_intf.DetectorComm
import org.junit.Before
import org.junit.Test

import java.io.BufferedInputStream
import java.io.File
import java.io.FileInputStream

/**
 * Copyright 2018 IEAP CTU
 * Author: Jakub Begera (jakub.begera@cvut.cz)
 */
class PluginLoaderTest {

    private var jar: File? = null
    private var bytes: ByteArray? = null

    @Before
    @Throws(Exception::class)
    fun setUp() {
        jar = File("../detector_communication_katherine/build/libs/kathrine_communication-0.0.0.1.jar")
        val inputStream = BufferedInputStream(FileInputStream(jar!!))
        bytes = inputStream.readAllBytes()
    }

    @Test
    @Throws(Exception::class)
    fun testLoad() {
        val pluginLoader = PluginLoader<DetectorComm, Any>()
        pluginLoader.init(bytes)
    }

    @Test
    @Throws(Exception::class)
    fun loadDetectorImplAndSetConfig() {
        val pluginLoader = PluginLoader<DetectorComm, Any>()
        pluginLoader.init(bytes)
        val detector = pluginLoader.getPlugin1()
        detector.setCallback { pluginLoader.classLoader }
        detector.detectorConfig = KATHRINE_CONFIG
    }

    companion object {

        private const val KATHRINE_CONFIG = "# Connection parameters\n" +
                "host: 192.168.1.122\n" +
                "portCommands: 1555\n" +
                "portData: 1556"
    }

}